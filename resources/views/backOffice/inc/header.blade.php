

<div class="main-header">
    <div class="navbar-header ml-3">
        <div class="navbar-brand">
            <a href="{{ route('showHome') }}">
                <style>
                    .nav-logo{
                        width: 154px;
                        height: 60px
                    }
                    
                </style>
              <img class="dark-logo nav-logo" src="{{asset('img/sahab-logo.png')}}" alt="platform sahab" >
            </a>
        </div>
    </div>
 



 
    <div class="menu-toggle">
        <div></div>
        <div></div>
        <div></div>
    </div>


    <div class="d-flex align-items-center">
    </div>

    <div style="margin: auto"></div>

    <div class="header-part-right">
        <!-- Full screen toggle -->
        <i class="i-Full-Screen header-icon d-none d-sm-inline-block" data-fullscreen></i>
        <!-- Grid menu Dropdown -->

        <!-- User avatar dropdown -->
    <a href="{{route('showHome')}}" class="  " > <i class="i-Home-4 header-icon d-none d-sm-inline-block"></i> </a>
        <div class="dropdown">
            <div  class="user col align-self-end">
                <img  src="{{ asset('img/unknown.png') }}"  id="userDropdown" alt="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                    <div class="dropdown-header">
                        <i class="i-Lock-User mr-1"></i> j.iheb
                    </div>
                    <a class="dropdown-item" href="{{route('showHome')}}">{{ trans('lang.back_home') }}</a>
                    <a class="dropdown-item" href="#">{{ trans('lang.logout') }}</a>
                </div>
            </div>
        </div>
    </div>

</div>