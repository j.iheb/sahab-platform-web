<div class="side-content-wrap">
    <div class="sidebar-left open" data-perfect-scrollbar data-suppress-scroll-x="true">
        <ul class="navigation-left navigation-main">
            <li class="nav-item @if($current == 'dashboard') active @endif">
                <a class="nav-item-hold" href="{{route('showManagerHome')}}">
                    <i class="nav-icon i-Bar-Chart"></i>
                    <span class="nav-text">{{ trans('lang.home') }}</span>
                </a>
                <div class="triangle"></div>
            </li>
            <li class="nav-item @if($current == 'consultations') active @endif">
                <a class="nav-item-hold" href="{{route('showManagerCourses')}}">
                    <i class="nav-icon i-Book"></i>
                    <span class="nav-text">الدورات</span>
                </a>
                <div class="triangle"></div>
            </li>
            <li class="nav-item @if($current == 'users') active @endif">
                <a class="nav-item-hold" href="{{route('showManagerUsers')}}">
                    <i class="nav-icon i-Find-User"></i>
                    <span class="nav-text">المستعملين</span>
                </a>
                <div class="triangle"></div>
            </li>

            <li class="nav-item @if($current == 'paths') active @endif">
                <a class="nav-item-hold" href="{{route('showManagerUsers')}}">
                    <i class="nav-icon i-Paper-Plane"></i>
                    <span class="nav-text">مسارات</span>
                </a>
                <div class="triangle"></div>
            </li>


            <li class="nav-item @if($current == 'projects') active @endif">
                <a class="nav-item-hold" href="{{route('showManagerUsers')}}">
                    <i class="nav-icon i-Coding"></i>
                    <span class="nav-text">مشاريع</span>
                </a>
                <div class="triangle"></div>
            </li>
        </ul>
    </div>

    <div class="sidebar-left-secondary" data-perfect-scrollbar data-suppress-scroll-x="true">
        <!-- Submenu Dashboards -->
        <ul class="childNav" data-parent="courses">
            <li class="nav-item">
                <a class="" href="#">
                    <i class="nav-icon i-Loop"></i>
                    <span class="item-name">{{ trans('lang.under_review') }}</span>
                </a>
            </li>

        </ul>
        <ul class="childNav" data-parent="courses">
            <li class="nav-item">
                <a class="" href="#">
                    <i class="nav-icon i-Full-View-Window"></i>
                    <span class="item-name">{{ trans('lang.online') }}</span>
                </a>
            </li>

        </ul>
        <ul class="childNav" data-parent="webinars">
            <li class="nav-item">
                <a class="" href="#">
                    <i class="nav-icon i-Loop"></i>
                    <span class="item-name">{{ trans('lang.under_review') }}</span>
                </a>
            </li>

        </ul>
        <ul class="childNav" data-parent="webinars">
            <li class="nav-item">
                <a class="" href="#">
                    <i class="nav-icon i-Full-View-Window"></i>
                    <span class="item-name">{{ trans('lang.online') }}</span>
                </a>
            </li>

        </ul>

    </div>

    <div class="sidebar-overlay"></div>
</div>
