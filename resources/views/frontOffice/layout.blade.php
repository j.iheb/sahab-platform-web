
<!DOCTYPE html>

<html lang="ar" dir="rtl">

@yield('head')

<body class="home page-template-default page page-id-437 wp-embed-responsive theme-edumall pmpro-body-has-access woocommerce-no-js desktop desktop-menu edumall-light-scheme mobile-menu-push-to-left woocommerce header-sticky-both wide page-has-no-sidebar title-bar-none elementor-default elementor-kit-47 elementor-page elementor-page-437" data-site-width="1200px" data-content-width="1200" data-font="Gordita Helvetica, Arial, sans-serif" data-header-sticky-height="80" style="direction:rtl; ">




@yield('header')


@yield('content')

@yield('footer')

@include('frontOffice.inc.scripts')

</body>

</html>
