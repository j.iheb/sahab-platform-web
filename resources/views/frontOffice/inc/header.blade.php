        <!-- header -->

        <header id="page-header" class="page-header header-02 header-dark nav-links-hover-style-01 header-sticky-dark-logo header-right">
                <div class="page-header-place-holder"></div>
                <div id="page-header-inner" class="page-header-inner" data-sticky="1">
                

                    <div class="container">
                        <div class="header-wrap">

                            <div class="header-left">
                                <div class="header-content-inner">
                                    <div class="branding">
                                        <div class="branding-logo-wrap">
                                            <a href="{{route('showHome')}}" rel="home">
                                                <img src="{{asset('img/sahab-logo.png')}}" alt="Main" class="branding-logo dark-logo">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="header-category-menu">
                                <a href="#" class="header-icon category-menu-toggle">
                              
                                    <div class="category-toggle-text">
                                        تصنيفات 
                                    </div>


                                    <div class="category-toggle-icon">
                                        <svg width="18px" height="18px" viewBox="0 0 18 18" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                            <g stroke="none" stroke-width="1" fill="#000000" fill-rule="evenodd">
                                                <path d="M2,14 C3.1045695,14 4,14.8954305 4,16 C4,17.1045695 3.1045695,18 2,18 C0.8954305,18 0,17.1045695 0,16 C0,14.8954305 0.8954305,14 2,14 Z M9,14 C10.1045695,14 11,14.8954305 11,16 C11,17.1045695 10.1045695,18 9,18 C7.8954305,18 7,17.1045695 7,16 C7,14.8954305 7.8954305,14 9,14 Z M16,14 C17.1045695,14 18,14.8954305 18,16 C18,17.1045695 17.1045695,18 16,18 C14.8954305,18 14,17.1045695 14,16 C14,14.8954305 14.8954305,14 16,14 Z M2,7 C3.1045695,7 4,7.8954305 4,9 C4,10.1045695 3.1045695,11 2,11 C0.8954305,11 0,10.1045695 0,9 C0,7.8954305 0.8954305,7 2,7 Z M9,7 C10.1045695,7 11,7.8954305 11,9 C11,10.1045695 10.1045695,11 9,11 C7.8954305,11 7,10.1045695 7,9 C7,7.8954305 7.8954305,7 9,7 Z M16,7 C17.1045695,7 18,7.8954305 18,9 C18,10.1045695 17.1045695,11 16,11 C14.8954305,11 14,10.1045695 14,9 C14,7.8954305 14.8954305,7 16,7 Z M2,0 C3.1045695,0 4,0.8954305 4,2 C4,3.1045695 3.1045695,4 2,4 C0.8954305,4 0,3.1045695 0,2 C0,0.8954305 0.8954305,0 2,0 Z M9,0 C10.1045695,0 11,0.8954305 11,2 C11,3.1045695 10.1045695,4 9,4 C7.8954305,4 7,3.1045695 7,2 C7,0.8954305 7.8954305,0 9,0 Z M16,0 C17.1045695,0 18,0.8954305 18,2 C18,3.1045695 17.1045695,4 16,4 C14.8954305,4 14,3.1045695 14,2 C14,0.8954305 14.8954305,0 16,0 Z"></path>
                                            </g>
                                        </svg>
                                    </div>

                                </a>

                                <nav class="header-category-dropdown-wrap">
                                    <ul class="header-category-dropdown">
                                        <li class="cat-item">
                                            <a href="#">
                                            
                                             فيزياء
                                             
                                             </a>
                                        </li>

                                        <li class="cat-item">
                                            <a href="#">
                                                برمجة 
                                            </a>

                                        </li>

                                        <li class="cat-item">
                                            <a href="#">
                                                تصميم  
                                            </a>

                                     
                                        </li>

                                        <li class="cat-item">
                                            <a href="#">
                                                تطوير 
                                            </a>

                                    
                                        </li>

                                        <li class="cat-item">
                                            <a href="#">
                                                تعليم الألة 
                                            </a>

                                     
                                        </li>

                                        <li class="cat-item">
                                            <a href="#">
                                               رياضيات
                                            </a>

                                       
                                        </li>

                                        <li class="cat-item">
                                            <a href="#">
                                                كيمياء
                                            </a>

                                          
                                        </li>

                                        <li class="cat-item">
                                            <a href="#">
                                                تسويق 
                                            </a>

                                          
                                        </li>

                                        <li class="cat-item">
                                            <a href="#">
                                                كتابة
                                            </a>

                                           
                                        </li>

                                    </ul>
                                </nav>
                            </div>

                            <div class="header-right" style="direction:rtl;">
                                <div class="header-content-inner" style="direction:rtl;">
                                    <div id="header-right-inner" class="header-right-inner" style="direction:rtl;">
                                        <div class="header-right-inner-content" style="direction:rtl;">

                                            <div class="header-search-form" style="direction:rtl;">
                                                <form role="search" method="get" class="search-form" action="#">
                                                 
                                                   <label>
                                                        <span class="screen-reader-text">إبحث عن :</span>
                                                        <input type="search" class="search-field" placeholder="إبحث ..." value="" name="s" title="Search for:" />
                                                    </label>

                                                    <button type="submit" class="search-submit">
                                                       
                                                       <span class="search-btn-text">
                                                           إبحث </span>
                                                           <span class="search-btn-icon fas fa-search"></span>  
                                                   </button>
                                                 
                                                 
                                                </form>
                                            </div>

                                            <div id="page-navigation" class="navigation page-navigation"  style="direction:rtl;">
                                                <nav id="menu" class="menu menu--primary" style="direction:rtl;">
                                                    <ul id="menu-primary" class="menu__container sm sm-simple" style="direction:rtl;">
                                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1718 level-1">
                                                            <a href="{{route('showHome')}}">
                                                                <div class="menu-item-wrap"><span class="menu-item-title">الرئيسية</span></div>
                                                            </a>
                                                         </li>

                                                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1718 level-1">
                                                                <a href="{{route('showCourses')}}">
                                                                    <div class="menu-item-wrap"><span class="menu-item-title">الدورات</span></div>
                                                                </a>
                                                            </li>

                                                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1718 level-1">
                                                                <a href="{{route('showProjects')}}">
                                                                    <div class="menu-item-wrap"><span class="menu-item-title">المشاريع</span></div>
                                                                </a>
                                                            </li>

                                                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1718 level-1">
                                                                <a href="{{route('showManagerHome')}}">
                                                                    <div class="menu-item-wrap"><span class="menu-item-title">لوحة التحكم</span></div>
                                                                </a>
                                                            </li>
                                                    </ul>
                                                </nav>
                                            </div>




                                            <div id="mini-cart" class="mini-cart style-normal" style="direction:rtl;">
                                                <a href="#" class="mini-cart__button header-icon" title="View your shopping cart" style="direction:rtl;">
                                                  <span class="mini-cart-icon" data-count="0"></span>
                                                </a>
                                                <div class="widget_shopping_cart_content"></div>
                                            </div>

                                            <div class="header-user-buttons">
                                                <div class="inner">
                                                    <div class="tm-button-wrapper"> <a class="tm-button style-bottom-line-alt button-thin tm-button-sm" href="{{route('showLogin')}}">
                                                            <div class="button-content-wrapper">


                                                                <span class="button-text">تسجيل الدخول</span>

                                                            </div>
                                                        </a> </div>
                                                    <div class="tm-button-wrapper"> <a class="tm-button style-flat tm-button-sm  button-thin button-light-primary" href="{{route('showRegister')}}">
                                                            <div class="button-content-wrapper">


                                                                <span class="button-text">مستخدم جديد</span>

                                                            </div>
                                                        </a> </div>
                                                </div>
                                            </div>


                                        </div>
                                    </div>

                                    <div id="page-open-mobile-menu" class="header-icon page-open-mobile-menu">
                                        <div class="burger-icon">
                                            <span class="burger-icon-top"></span>
                                            <span class="burger-icon-bottom"></span>
                                        </div>
                                    </div>

                                    <div id="page-open-components" class="header-icon page-open-components">
                                        <div class="inner">
                                            <div class="circle circle-one"></div>
                                            <div class="circle circle-two"></div>
                                            <div class="circle circle-three"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </header>

        <!--  /header  -->
