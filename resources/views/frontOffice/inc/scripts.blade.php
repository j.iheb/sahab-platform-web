  <!-- Memberships powered by Paid Memberships Pro v2.5.2.
 -->
 <div class="wooscp-popup wooscp-search">
        <div class="wooscp-popup-inner">
            <div class="wooscp-popup-content">
                <div class="wooscp-popup-content-inner">
                    <div class="wooscp-popup-close"></div>
                    <div class="wooscp-search-input">
                        <input type="search" id="wooscp_search_input" placeholder="Type any keyword to search..." />
                    </div>
                    <div class="wooscp-search-result"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="wooscp-popup wooscp-settings">
        <div class="wooscp-popup-inner">
            <div class="wooscp-popup-content">
                <div class="wooscp-popup-content-inner">
                    <div class="wooscp-popup-close"></div>
                    Select the fields to be shown. Others will be hidden. Drag and drop to rearrange the order. <ul class="wooscp-settings-fields">
                        <li class="wooscp-settings-field-li"><input type="checkbox" class="wooscp-settings-field" value="image" checked /><span class="label">Image</span></li>
                        <li class="wooscp-settings-field-li"><input type="checkbox" class="wooscp-settings-field" value="sku" checked /><span class="label">SKU</span></li>
                        <li class="wooscp-settings-field-li"><input type="checkbox" class="wooscp-settings-field" value="rating" checked /><span class="label">Rating</span></li>
                        <li class="wooscp-settings-field-li"><input type="checkbox" class="wooscp-settings-field" value="price" checked /><span class="label">Price</span></li>
                        <li class="wooscp-settings-field-li"><input type="checkbox" class="wooscp-settings-field" value="stock" checked /><span class="label">Stock</span></li>
                        <li class="wooscp-settings-field-li"><input type="checkbox" class="wooscp-settings-field" value="availability" checked /><span class="label">Availability</span></li>
                        <li class="wooscp-settings-field-li"><input type="checkbox" class="wooscp-settings-field" value="add_to_cart" checked /><span class="label">Add to cart</span></li>
                        <li class="wooscp-settings-field-li"><input type="checkbox" class="wooscp-settings-field" value="description" checked /><span class="label">Description</span></li>
                        <li class="wooscp-settings-field-li"><input type="checkbox" class="wooscp-settings-field" value="content" checked /><span class="label">Content</span></li>
                        <li class="wooscp-settings-field-li"><input type="checkbox" class="wooscp-settings-field" value="weight" checked /><span class="label">Weight</span></li>
                        <li class="wooscp-settings-field-li"><input type="checkbox" class="wooscp-settings-field" value="dimensions" checked /><span class="label">Dimensions</span></li>
                        <li class="wooscp-settings-field-li"><input type="checkbox" class="wooscp-settings-field" value="additional" checked /><span class="label">Additional information</span></li>
                        <li class="wooscp-settings-field-li"><input type="checkbox" class="wooscp-settings-field" value="attributes" checked /><span class="label">Attributes</span></li>
                        <li class="wooscp-settings-field-li"><input type="checkbox" class="wooscp-settings-field" value="custom_attributes" checked /><span class="label">Custom attributes</span></li>
                        <li class="wooscp-settings-field-li"><input type="checkbox" class="wooscp-settings-field" value="custom_fields" checked /><span class="label">Custom fields</span></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div id="wooscp-area" class="wooscp-area wooscp-bar-bottom wooscp-bar-right wooscp-hide-checkout" data-bg-color="#292a30" data-btn-color="#0071dc">
        <div class="wooscp-inner">
            <div class="wooscp-table">
                <div class="wooscp-table-inner">
                    <a href="javascript:void(0);" id="wooscp-table-close" class="wooscp-table-close hint--left" aria-label="Close"><span class="wooscp-table-close-icon"></span></a>
                    <div class="wooscp-table-items"></div>
                </div>
            </div>
            <div class="wooscp-bar">
                <a href="javascript:void(0);" class="wooscp-bar-settings hint--top" aria-label="Select fields"></a>
                <a href="javascript:void(0);" class="wooscp-bar-search hint--top" aria-label="Add product"></a>
                <div class="wooscp-bar-items"></div>
                <div class="wooscp-bar-btn wooscp-bar-btn-text">
                    <div class="wooscp-bar-btn-icon-wrapper">
                        <div class="wooscp-bar-btn-icon-inner"><span></span><span></span><span></span>
                        </div>
                    </div>
                    Compare
                </div>
            </div>
        </div>
    </div>
    <div id="woosw-area" class="woosw-area">
        <div class="woosw-inner">
            <div class="woosw-content">
                <div class="woosw-content-top">
                    Wishlist <span class="woosw-count">0</span>
                    <span class="woosw-close"></span>
                </div>
                <div class="woosw-content-mid"></div>
                <div class="woosw-content-bot">
                    <div class="woosw-content-bot-inner">
                        <span class="woosw-page">
                            <a href="https://live-edumall.thememove.com/wishlist/">Open wishlist page</a>
                        </span>
                        <span class="woosw-continue" data-url="">
                            Continue shopping </span>
                    </div>
                    <div class="woosw-notice"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="tm-demo-options-wrapper">

        <div id="tm-demo-panel" class="tm-demo-panel">
            <div class="tm-demo-panel-header">
                <h5 class="demo-option-title">
                    EduMall - Education WordPress Theme </h5>


                <a class="tm-button style-flat tm-button-sm tm-button-primary tm-btn-purchase has-icon icon-left edumall-gg-track" href="https://themeforest.net/item/edumall-lms-education-wordpress-theme/29240444" target="_blank">
                    <span class="button-icon">
                        <i class="far fa-shopping-cart"></i>
                    </span>
                    <span class="button-text">
                        Buy Now </span>
                </a>
            </div>

            <div class="quick-option-list">
                <a href="https://edumall.thememove.com" class="hint--bounce hint--top" aria-label="Landing Page">
                    <div class="edumall-skeleton-item" data-src="https://live-edumall.thememove.com/wp-content/themes/edumall-child-demo/assets/images/preview/landing.jpg" data-alt="Landing Page"></div>
                </a>
                <a href="https://live-edumall.thememove.com" class="hint--bounce hint--top" aria-label="Main Demo">
                    <div class="edumall-skeleton-item" data-src="https://live-edumall.thememove.com/wp-content/themes/edumall-child-demo/assets/images/preview/home-01.jpg" data-alt="Main Demo"></div>
                </a>
                <a href="https://live-edumall.thememove.com/course-hub" class="hint--bounce hint--top" aria-label="Course Hub">
                    <div class="edumall-skeleton-item" data-src="https://live-edumall.thememove.com/wp-content/themes/edumall-child-demo/assets/images/preview/home-02.jpg" data-alt="Course Hub"></div>
                </a>
                <a href="https://live-edumall.thememove.com/online-academy" class="hint--bounce hint--top" aria-label="Online Academy">
                    <div class="edumall-skeleton-item" data-src="https://live-edumall.thememove.com/wp-content/themes/edumall-child-demo/assets/images/preview/home-03.jpg" data-alt="Online Academy"></div>
                </a>
                <a href="https://live-edumall.thememove.com/education-center" class="hint--bounce hint--top" aria-label="Education Center">
                    <div class="edumall-skeleton-item" data-src="https://live-edumall.thememove.com/wp-content/themes/edumall-child-demo/assets/images/preview/home-education-center.jpg" data-alt="Education Center"></div>
                </a>
                <a href="https://live-edumall.thememove.com/university" class="hint--bounce hint--top" aria-label="University">
                    <div class="edumall-skeleton-item" data-src="https://live-edumall.thememove.com/wp-content/themes/edumall-child-demo/assets/images/preview/home-university.jpg" data-alt="University"></div>
                </a>
                <a href="https://live-edumall.thememove.com/language-academic" class="hint--bounce hint--top" aria-label="Language Academic">
                    <div class="edumall-skeleton-item" data-src="https://live-edumall.thememove.com/wp-content/themes/edumall-child-demo/assets/images/preview/home-language-academic.jpg" data-alt="Language Academic"></div>
                </a>
                <a href="https://live-edumall.thememove.com/single-instructor" class="hint--bounce hint--top" aria-label="Single Instructor">
                    <div class="edumall-skeleton-item" data-src="https://live-edumall.thememove.com/wp-content/themes/edumall-child-demo/assets/images/preview/home-single-instructor.jpg" data-alt="Single Instructor"></div>
                </a>
                <a href="https://live-edumall.thememove.com/dev" class="hint--bounce hint--top" aria-label="Dev">
                    <div class="edumall-skeleton-item" data-src="https://live-edumall.thememove.com/wp-content/themes/edumall-child-demo/assets/images/preview/home-dev.jpg" data-alt="Dev"></div>
                </a>
                <a href="https://live-edumall.thememove.com/online-art" class="hint--bounce hint--top" aria-label="Online Art">
                    <div class="edumall-skeleton-item" data-src="https://live-edumall.thememove.com/wp-content/themes/edumall-child-demo/assets/images/preview/home-online-art.jpg" data-alt="Online Art"></div>
                </a>
                <a href="https://edumall.thememove.com/dark" class="hint--bounce hint--top" aria-label="Dark Skin">
                    <div class="edumall-skeleton-item" data-src="https://live-edumall.thememove.com/wp-content/themes/edumall-child-demo/assets/images/preview/home-dark.jpg" data-alt="Dark Skin"></div>
                </a>
                <a href="https://edumall.thememove.com/community" class="hint--bounce hint--top" aria-label="Community (BuddyPress)">
                    <div class="edumall-skeleton-item" data-src="https://live-edumall.thememove.com/wp-content/themes/edumall-child-demo/assets/images/preview/community.jpg" data-alt="Community (BuddyPress)"></div>
                </a>
                <a href="https://edumall.thememove.com/rtl" class="hint--bounce hint--top" aria-label="RTL Demo">
                    <div class="edumall-skeleton-item" data-src="https://live-edumall.thememove.com/wp-content/themes/edumall-child-demo/assets/images/preview/rtl.jpg" data-alt="RTL Demo"></div>
                </a>
            </div>
        </div>
    </div>
    <script>
        jQuery(document).ready(function($) {
            'use strict';
            $('#toggle-quick-options').on('click', function(e) {
                e.preventDefault();
                var $parent = $(this).parents('.tm-demo-options-wrapper');

                $parent.toggleClass('open');

                if (!$parent.hasClass('loaded')) {
                    $parent.addClass('loaded');

                    var $demoList = $parent.find('.quick-option-list').children('a');

                    $demoList.each(function() {
                        var $link = $(this);
                        var $placeholderImg = $(this).children('.edumall-skeleton-item');

                        var image = $('<img />', {
                            src: $placeholderImg.data('src'),
                            alt: $placeholderImg.data('alt')
                        });

                        $link.html(image);
                    });
                }
            });
        });
    </script>
    <!--
    <div id="popup-try-live-demo" class="popup-try-live-demo hidden">
        <span id="popup-try-live-demo-close" class="popup-try-live-demo-close"><i class="far fa-times"></i></span>
        <h4 class="popup-try-live-demo-heading">Login &amp; try EduMall</h4>
        <p class="popup-try-live-demo-description">without sign up any accounts</p>
        <form method="post" class="try-live-demo-form">
            <input type="hidden" id="edumall_live_login_nonce" name="edumall_live_login_nonce" value="781498644c" /><input type="hidden" name="_wp_http_referer" value="/" /> <input type="hidden" name="action" value="edumall_live_login">
            <input type="hidden" name="type" value="student">
        </form>
        <a class="tm-button style-flat tm-button-nm tm-button-full-wide login-as-student ajax-login" id="edumall-live-login-as-student" href="https://live-edumall.thememove.com" target="_blank" rel="nofollow">
            <div class="button-content-wrapper">


                <span class="button-text">Login as Student</span>

            </div>
        </a> <a class="tm-button style-flat tm-button-nm tm-button-full-wide login-as-instructor ajax-login" id="edumall-live-login-as-instructor" href="https://live-edumall.thememove.com" target="_blank" rel="nofollow">
            <div class="button-content-wrapper">


                <span class="button-text">Login as Instructor</span>

            </div>
        </a>
    </div>
    -->
    <script>
        jQuery(document).ready(function($) {
            'use strict';

            // Retrieve.
            var popupOn = true;
            var $popupTryLiveDemo = $('#popup-try-live-demo');
            var $form = $popupTryLiveDemo.children('form');

            $popupTryLiveDemo.children('.login-as-student').addClass('tm-button-bounce');
            setTimeout(function() {
                $popupTryLiveDemo.children('.login-as-instructor').addClass('tm-button-bounce');
            }, 2000);

            $popupTryLiveDemo.on('click', '.tm-button', function(e) {
                var $button = $(this);

                if ($button.hasClass('ajax-login')) {
                    e.preventDefault();
                    e.stopPropagation();

                    if ($button.hasClass('login-as-instructor')) {
                        $form.children('input[name=type]').val('instructor');
                    } else {
                        $form.children('input[name=type]').val('student');
                    }

                    $.ajax({
                        url: $edumall.ajaxurl,
                        type: 'POST',
                        dataType: 'json',
                        data: $form.serialize(),
                        success: function(response) {
                            if ('' !== response.redirect_url) {
                                window.location.href = response.redirect_url;
                            }
                        },
                    });
                }
            });

            if (typeof(
                    Storage
                ) !== "undefined" && sessionStorage.getItem('popup_try_live_demo_disabled') === '1') {
                popupOn = false;
            }

            if (popupOn === true) {
                $popupTryLiveDemo.removeClass('hidden')
            }

            $('#popup-try-live-demo-close').on('click', function(e) {
                e.preventDefault();
                e.stopPropagation();

                if (typeof(
                        Storage
                    ) !== "undefined") {
                    sessionStorage.setItem('popup_try_live_demo_disabled', '1');
                }

                $popupTryLiveDemo.addClass('hidden');
            });
        });
    </script>
    <div id="popup-pre-loader" class="popup-pre-loader">
        <div class="popup-load-inner">
            <div class="popup-loader-wrap">
                <div class="wrap-2">
                    <div class="inner">
                        <div class="sk-wrap sk-circle">
                            <div class="sk-circle1 sk-child"></div>
                            <div class="sk-circle2 sk-child"></div>
                            <div class="sk-circle3 sk-child"></div>
                            <div class="sk-circle4 sk-child"></div>
                            <div class="sk-circle5 sk-child"></div>
                            <div class="sk-circle6 sk-child"></div>
                            <div class="sk-circle7 sk-child"></div>
                            <div class="sk-circle8 sk-child"></div>
                            <div class="sk-circle9 sk-child"></div>
                            <div class="sk-circle10 sk-child"></div>
                            <div class="sk-circle11 sk-child"></div>
                            <div class="sk-circle12 sk-child"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="edumall-popup popup-user-login" id="popup-user-login" data-template="template-parts/popup/popup-content-login">
        <div class="popup-overlay"></div>
        <div class="popup-content">
            <div class="button-close-popup"></div>
            <div class="popup-content-wrap">
                <div class="popup-content-inner"></div>
            </div>
        </div>
    </div>

    <div class="edumall-popup popup-user-register" id="popup-user-register" data-template="template-parts/popup/popup-content-register">
        <div class="popup-overlay"></div>
        <div class="popup-content">
            <div class="button-close-popup"></div>
            <div class="popup-content-wrap">
                <div class="popup-content-inner"></div>
            </div>
        </div>
    </div>

    <div class="edumall-popup popup-lost-password" id="popup-user-lost-password" data-template="template-parts/popup/popup-content-lost-password">
        <div class="popup-overlay"></div>
        <div class="popup-content">
            <div class="button-close-popup"></div>
            <div class="popup-content-wrap">
                <div class="popup-content-inner"></div>
            </div>
        </div>
    </div>
    <a class="page-scroll-up" id="page-scroll-up">
        <i class="arrow-top fal fa-long-arrow-up"></i>
        <i class="arrow-bottom fal fa-long-arrow-up"></i>
    </a>
    <div id="page-mobile-main-menu" class="page-mobile-main-menu" data-background="https://live-edumall.thememove.com/wp-content/themes/edumall/assets/images/mobile-bg.jpg" style="direction:rtl;">
        <div class="inner">
            <div class="page-mobile-menu-header">
                <div class="page-mobile-popup-logo page-mobile-menu-logo">
                    <a href="https://live-edumall.thememove.com/" rel="home">
                        <img src="{{asset('img/sahab-logo.png')}}" alt="Main" width="148" />
                    </a>
                </div>
                <div id="page-close-mobile-menu" class="page-close-mobile-menu">
                    <div class="burger-icon burger-icon-close">
                        <span class="burger-icon-top"></span>
                        <span class="burger-icon-bottom"></span>
                    </div>
                </div>
            </div>

            <div class="page-mobile-menu-content">
                <ul id="mobile-menu-primary" class="menu__container">
                    <li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-has-children menu-item-616 level-1 has-mega-menu"><a href="https://live-edumall.thememove.com/">
                            <div class="menu-item-wrap"><span class="menu-item-title">الرئيسية</span></div>
                        </a>
                       
                    </li>
                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1718 level-1"><a href="https://live-edumall.thememove.com/become-an-instructor/">
                            <div class="menu-item-wrap"><span class="menu-item-title">الدورات</span></div>
                        </a></li>

                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1718 level-1"><a href="https://live-edumall.thememove.com/become-an-instructor/">
                            <div class="menu-item-wrap"><span class="menu-item-title">المشاريع</span></div>
                        </a></li>   
                </ul>
            </div>
        </div>
    </div>
    <div class="edumall-popup popup-instructor-registration" id="edumall-popup-instructor-register" data-template="tutor/global/popup-content-instructor-registration">
        <div class="popup-overlay"></div>
        <div class="popup-content">
            <div class="button-close-popup"></div>
            <div class="popup-content-wrap">
                <div class="popup-content-inner"></div>
            </div>
        </div>
    </div>
    <script>
        (function() {
            var c = document.body.className;
            c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
            document.body.className = c;
        })()
    </script>
    <link rel='stylesheet' id='elementor-post-2387-css' href='https://live-edumall.thememove.com/wp-content/uploads/elementor/css/post-2387.css' media='all' />
    <script src='https://live-edumall.thememove.com/wp-content/themes/edumall/assets/libs/polyfill/intersection-observer.min.js' id='intersection-observer-js'></script>
    <script id='quicktags-js-extra'>
        /* <![CDATA[ */
        var quicktagsL10n = {
            "closeAllOpenTags": "Close all open tags",
            "closeTags": "close tags",
            "enterURL": "Enter the URL",
            "enterImageURL": "Enter the URL of the image",
            "enterImageDescription": "Enter a description of the image",
            "textdirection": "text direction",
            "toggleTextdirection": "Toggle Editor Text Direction",
            "dfw": "Distraction-free writing mode",
            "strong": "Bold",
            "strongClose": "Close bold tag",
            "em": "Italic",
            "emClose": "Close italic tag",
            "link": "Insert link",
            "blockquote": "Blockquote",
            "blockquoteClose": "Close blockquote tag",
            "del": "Deleted text (strikethrough)",
            "delClose": "Close deleted text tag",
            "ins": "Inserted text",
            "insClose": "Close inserted text tag",
            "image": "Insert image",
            "ul": "Bulleted list",
            "ulClose": "Close bulleted list tag",
            "ol": "Numbered list",
            "olClose": "Close numbered list tag",
            "li": "List item",
            "liClose": "Close list item tag",
            "code": "Code",
            "codeClose": "Close code tag",
            "more": "Insert Read More tag"
        };
        /* ]]> */
    </script>
    <script src='https://live-edumall.thememove.com/wp-includes/js/quicktags.min.js' id='quicktags-js'></script>
    <script src='https://live-edumall.thememove.com/wp-includes/js/jquery/ui/core.min.js' id='jquery-ui-core-js'></script>
    <script src='https://live-edumall.thememove.com/wp-includes/js/jquery/ui/mouse.min.js' id='jquery-ui-mouse-js'></script>
    <script src='https://live-edumall.thememove.com/wp-includes/js/jquery/ui/sortable.min.js' id='jquery-ui-sortable-js'></script>
    <script src='https://live-edumall.thememove.com/wp-content/plugins/tutor/assets/packages/plyr/plyr.polyfilled.min.js' id='tutor-plyr-js'></script>
    <script src='https://live-edumall.thememove.com/wp-content/plugins/tutor/assets/js/tutor.js' id='tutor-main-js'></script>
    <script id='tutor-frontend-js-extra'>
        /* <![CDATA[ */
        var _tutorobject = {
            "ajaxurl": "https:\/\/live-edumall.thememove.com\/wp-admin\/admin-ajax.php",
            "nonce_key": "_wpnonce",
            "_wpnonce": "a40350722a",
            "placeholder_img_src": "https:\/\/live-edumall.thememove.com\/wp-content\/plugins\/tutor\/assets\/images\/placeholder.jpg",
            "enable_lesson_classic_editor": "",
            "text": {
                "assignment_text_validation_msg": "Assignment answer can not be empty"
            }
        };
        /* ]]> */
    </script>
    <script src='https://live-edumall.thememove.com/wp-content/plugins/tutor/assets/js/tutor-front.js' id='tutor-frontend-js'></script>
    <script src='https://live-edumall.thememove.com/wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min.js' id='jquery-blockui-js'></script>
    <script id='wc-add-to-cart-js-extra'>
        /* <![CDATA[ */
        var wc_add_to_cart_params = {
            "ajax_url": "\/wp-admin\/admin-ajax.php",
            "wc_ajax_url": "\/?wc-ajax=%%endpoint%%",
            "i18n_view_cart": "View cart",
            "cart_url": "https:\/\/live-edumall.thememove.com\/cart\/",
            "is_cart": "",
            "cart_redirect_after_add": "no"
        };
        /* ]]> */
    </script>
    <script src='https://live-edumall.thememove.com/wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart.min.js' id='wc-add-to-cart-js'></script>
    <script src='https://live-edumall.thememove.com/wp-content/plugins/woocommerce/assets/js/js-cookie/js.cookie.min.js' id='js-cookie-js'></script>
    <script id='woocommerce-js-extra'>
        /* <![CDATA[ */
        var woocommerce_params = {
            "ajax_url": "\/wp-admin\/admin-ajax.php",
            "wc_ajax_url": "\/?wc-ajax=%%endpoint%%"
        };
        /* ]]> */
    </script>
    <script src='https://live-edumall.thememove.com/wp-content/plugins/woocommerce/assets/js/frontend/woocommerce.min.js' id='woocommerce-js'></script>
    <script id='wc-cart-fragments-js-extra'>
        /* <![CDATA[ */
        var wc_cart_fragments_params = {
            "ajax_url": "\/wp-admin\/admin-ajax.php",
            "wc_ajax_url": "\/?wc-ajax=%%endpoint%%",
            "cart_hash_key": "wc_cart_hash_c2624d7e250687d6d44b0c772c891a01",
            "fragment_name": "wc_fragments_c2624d7e250687d6d44b0c772c891a01",
            "request_timeout": "5000"
        };
        /* ]]> */
    </script>
    <script src='https://live-edumall.thememove.com/wp-content/plugins/woocommerce/assets/js/frontend/cart-fragments.min.js' id='wc-cart-fragments-js'></script>
    <script src='https://live-edumall.thememove.com/wp-includes/js/underscore.min.js' id='underscore-js'></script>
    <script id='wp-util-js-extra'>
        /* <![CDATA[ */
        var _wpUtilSettings = {
            "ajax": {
                "url": "\/wp-admin\/admin-ajax.php"
            }
        };
        /* ]]> */
    </script>
    <script src='https://live-edumall.thememove.com/wp-includes/js/wp-util.min.js' id='wp-util-js'></script>
    <script src='https://live-edumall.thememove.com/wp-includes/js/backbone.min.js' id='backbone-js'></script>
    <script src='https://live-edumall.thememove.com/wp-content/plugins/woo-smart-compare/assets/libs/dragarrange/drag-arrange.js' id='dragarrange-js'></script>
    <script src='https://live-edumall.thememove.com/wp-content/plugins/woo-smart-compare/assets/libs/table-head-fixer/table-head-fixer.js' id='table-head-fixer-js'></script>
    <script src='https://live-edumall.thememove.com/wp-content/plugins/woo-smart-compare/assets/libs/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js' id='perfect-scrollbar-js'></script>
    <script id='wooscp-frontend-js-extra'>
        /* <![CDATA[ */
        var wooscpVars = {
            "ajaxurl": "https:\/\/live-edumall.thememove.com\/wp-admin\/admin-ajax.php",
            "user_id": "5d683352529913f1f848a4c9e3f5b2b8",
            "page_url": "",
            "open_button": "",
            "open_button_action": "open_popup",
            "menu_action": "open_popup",
            "open_table": "yes",
            "open_bar": "no",
            "click_again": "no",
            "remove_all": "Do you want to remove all products from the compare?",
            "hide_empty": "no",
            "click_outside": "yes",
            "freeze_column": "yes",
            "freeze_row": "yes",
            "limit": "100",
            "limit_notice": "You can add a maximum of {limit} products to the compare table.",
            "button_text": "Compare",
            "button_text_added": "Compare",
            "nonce": "b4cd5770b4"
        };
        /* ]]> */
    </script>
    <script src='https://live-edumall.thememove.com/wp-content/plugins/woo-smart-compare/assets/js/frontend.js' id='wooscp-frontend-js'></script>
    <script id='woosw-frontend-js-extra'>
        /* <![CDATA[ */
        var woosw_vars = {
            "ajax_url": "https:\/\/live-edumall.thememove.com\/wp-admin\/admin-ajax.php",
            "menu_action": "open_page",
            "copied_text": "Copied the wishlist link:",
            "menu_text": "Wishlist",
            "wishlist_url": "https:\/\/live-edumall.thememove.com\/wishlist\/",
            "button_text": "Add to wishlist",
            "button_action": "list",
            "button_text_added": "Browse wishlist",
            "button_action_added": "popup"
        };
        /* ]]> */
    </script>
    <script src='https://live-edumall.thememove.com/wp-content/plugins/woo-smart-wishlist/assets/js/frontend.js' id='woosw-frontend-js'></script>
    <script src='https://live-edumall.thememove.com/wp-content/themes/edumall/assets/js/headroom.min.js' id='headroom-js'></script>
    <script src='https://live-edumall.thememove.com/wp-content/themes/edumall/assets/libs/smooth-scroll-for-web/SmoothScroll.min.js' id='smooth-scroll-js'></script>
    <script src='https://live-edumall.thememove.com/wp-content/themes/edumall/assets/libs/lightGallery/js/lightgallery-all.min.js' id='lightgallery-js'></script>
    <script src='https://live-edumall.thememove.com/wp-content/plugins/elementor/assets/lib/waypoints/waypoints.min.js' id='elementor-waypoints-js'></script>
    <script src='https://live-edumall.thememove.com/wp-content/themes/edumall/assets/libs/powertip/js/jquery.powertip.min.js' id='powertip-js'></script>
    <script src='https://live-edumall.thememove.com/wp-content/themes/edumall/assets/libs/smooth-scroll/jquery.smooth-scroll.min.js' id='jquery-smooth-scroll-js'></script>
    <script src='https://live-edumall.thememove.com/wp-includes/js/imagesloaded.min.js' id='imagesloaded-js'></script>
    <script src='https://live-edumall.thememove.com/wp-content/themes/edumall/assets/libs/swiper/js/swiper.min.js' id='swiper-js'></script>
    <script id='edumall-swiper-wrapper-js-extra'>
        /* <![CDATA[ */
        var $edumallSwiper = {
            "fractionPrefixText": "Show",
            "prevText": "Prev",
            "nextText": "Next"
        };
        var $edumallSwiper = {
            "fractionPrefixText": "Show",
            "prevText": "Prev",
            "nextText": "Next"
        };
        var $edumallSwiper = {
            "fractionPrefixText": "Show",
            "prevText": "Prev",
            "nextText": "Next"
        };
        /* ]]> */
    </script>
    <script src='https://live-edumall.thememove.com/wp-content/themes/edumall/assets/js/swiper-wrapper.min.js' id='edumall-swiper-wrapper-js'></script>
    <script src='https://live-edumall.thememove.com/wp-content/themes/edumall/assets/libs/matchHeight/jquery.matchHeight-min.js' id='matchheight-js'></script>
    <script src='https://live-edumall.thememove.com/wp-content/themes/edumall/assets/libs/isotope/js/isotope.pkgd.js' id='isotope-masonry-js'></script>
    <script src='https://live-edumall.thememove.com/wp-content/themes/edumall/assets/libs/packery-mode/packery-mode.pkgd.js' id='isotope-packery-js'></script>
    <script src='https://live-edumall.thememove.com/wp-content/themes/edumall/assets/js/grid-layout.min.js' id='edumall-grid-layout-js'></script>
    <script src='https://live-edumall.thememove.com/wp-content/plugins/elementor-pro/assets/lib/smartmenus/jquery.smartmenus.min.js' id='smartmenus-js'></script>
    <script src='https://live-edumall.thememove.com/wp-content/themes/edumall/assets/libs/growl/js/jquery.growl.min.js' id='growl-js'></script>
    <script src='https://live-edumall.thememove.com/wp-content/themes/edumall/assets/js/nice-select.min.js' id='edumall-nice-select-js'></script>
    <script id='edumall-script-js-extra'>
        /* <![CDATA[ */
        var $edumall = {
            "ajaxurl": "https:\/\/live-edumall.thememove.com\/wp-admin\/admin-ajax.php",
            "header_sticky_enable": "1",
            "header_sticky_height": "80",
            "scroll_top_enable": "1",
            "light_gallery_auto_play": "0",
            "light_gallery_download": "1",
            "light_gallery_full_screen": "1",
            "light_gallery_zoom": "1",
            "light_gallery_thumbnail": "1",
            "light_gallery_share": "1",
            "mobile_menu_breakpoint": "1199",
            "isRTL": "",
            "isProduct": "",
            "productFeatureStyle": "slider",
            "noticeCookieEnable": "0",
            "noticeCookieConfirm": "no",
            "noticeCookieMessages": "We use cookies to ensure that we give you the best experience on our website. If you continue to use this site we will assume that you are happy with it.<a id=\"tm-button-cookie-notice-ok\" class=\"tm-button tm-button-xs tm-button-full-wide style-flat\">Ok, got it!<\/a>",
            "noticeCartUrl": "https:\/\/live-edumall.thememove.com\/cart\/",
            "noticeCartText": "View Cart",
            "noticeAddedCartText": "added to cart!",
            "countdownDaysText": "Days",
            "countdownHoursText": "Hours",
            "countdownMinutesText": "Mins",
            "countdownSecondsText": "Secs"
        };
        /* ]]> */
    </script>
    <script src='https://live-edumall.thememove.com/wp-content/themes/edumall/assets/js/main.min.js' id='edumall-script-js'></script>
    <script src='https://live-edumall.thememove.com/wp-content/themes/edumall/assets/js/woo/general.min.js' id='edumall-woo-general-js'></script>
    <script id='isw-frontend-js-extra'>
        /* <![CDATA[ */
        var isw_vars = {
            "ajax": "https:\/\/live-edumall.thememove.com\/wp-admin\/admin-ajax.php",
            "product_selector": ".product",
            "price_selector": ".price",
            "localization": {
                "add_to_cart_text": "Add to cart",
                "read_more_text": "Read more",
                "select_options_text": "Select options"
            }
        };
        /* ]]> */
    </script>
    <script src='https://live-edumall.thememove.com/wp-content/plugins/insight-swatches/assets/js/scripts.js' id='isw-frontend-js'></script>
    <script src='https://live-edumall.thememove.com/wp-content/themes/edumall/assets/libs/validate/jquery.validate.min.js' id='validate-js'></script>
    <script id='edumall-login-js-extra'>
        /* <![CDATA[ */
        var $edumallLogin = {
            "validatorMessages": {
                "required": "This field is required",
                "remote": "Please fix this field",
                "email": "A valid email address is required",
                "url": "Please enter a valid URL",
                "date": "Please enter a valid date",
                "dateISO": "Please enter a valid date (ISO)",
                "number": "Please enter a valid number.",
                "digits": "Please enter only digits.",
                "creditcard": "Please enter a valid credit card number",
                "equalTo": "Please enter the same value again",
                "accept": "Please enter a value with a valid extension",
                "maxlength": "Please enter no more than {0} characters",
                "minlength": "Please enter at least {0} characters",
                "rangelength": "Please enter a value between {0} and {1} characters long",
                "range": "Please enter a value between {0} and {1}",
                "max": "Please enter a value less than or equal to {0}",
                "min": "Please enter a value greater than or equal to {0}"
            }
        };
        /* ]]> */
    </script>
    <script src='https://live-edumall.thememove.com/wp-content/themes/edumall/assets/js/login.min.js' id='edumall-login-js'></script>
    <script id='edumall-course-general-js-extra'>
        /* <![CDATA[ */
        var $edumallCourseWishlist = {
            "addedText": "Remove from wishlist",
            "addText": "Add to wishlist"
        };
        /* ]]> */
    </script>
    <script src='https://live-edumall.thememove.com/wp-content/themes/edumall/assets/js/tutor/general.min.js' id='edumall-course-general-js'></script>
    <script src='https://live-edumall.thememove.com/wp-content/themes/edumall/elementor/assets/libs/vivus/vivus.min.js' id='vivus-js'></script>
    <script src='https://live-edumall.thememove.com/wp-content/themes/edumall/elementor/assets/js/widgets/widget-icon-box.js' id='edumall-widget-icon-box-js'></script>
    <script src='https://live-edumall.thememove.com/wp-content/themes/edumall/elementor/assets/js/widgets/group-widget-grid.js' id='edumall-group-widget-grid-js'></script>
    <script src='https://live-edumall.thememove.com/wp-content/themes/edumall/assets/js/tab-panel.min.js' id='edumall-tab-panel-js'></script>
    <script src='https://live-edumall.thememove.com/wp-content/plugins/elementor/assets/js/frontend-modules.min.js' id='elementor-frontend-modules-js'></script>
    <script src='https://live-edumall.thememove.com/wp-content/plugins/elementor/assets/lib/dialog/dialog.min.js' id='elementor-dialog-js'></script>
    <script src='https://live-edumall.thememove.com/wp-content/plugins/elementor/assets/lib/share-link/share-link.min.js' id='share-link-js'></script>
    <script id='elementor-frontend-js-before'>
        var elementorFrontendConfig = {
            "environmentMode": {
                "edit": false,
                "wpPreview": false
            },
            "i18n": {
                "shareOnFacebook": "Share on Facebook",
                "shareOnTwitter": "Share on Twitter",
                "pinIt": "Pin it",
                "download": "Download",
                "downloadImage": "Download image",
                "fullscreen": "Fullscreen",
                "zoom": "Zoom",
                "share": "Share",
                "playVideo": "Play Video",
                "previous": "Previous",
                "next": "Next",
                "close": "Close"
            },
            "is_rtl": false,
            "breakpoints": {
                "xs": 0,
                "sm": 480,
                "md": 768,
                "lg": 1025,
                "xl": 1440,
                "xxl": 1600
            },
            "version": "3.0.14",
            "is_static": false,
            "legacyMode": {
                "elementWrappers": true
            },
            "urls": {
                "assets": "https:\/\/live-edumall.thememove.com\/wp-content\/plugins\/elementor\/assets\/"
            },
            "settings": {
                "page": [],
                "editorPreferences": []
            },
            "kit": {
                "global_image_lightbox": "yes",
                "lightbox_enable_counter": "yes",
                "lightbox_enable_fullscreen": "yes",
                "lightbox_enable_zoom": "yes",
                "lightbox_enable_share": "yes",
                "lightbox_title_src": "title",
                "lightbox_description_src": "description"
            },
            "post": {
                "id": 437,
                "title": "Main%20%E2%80%93%20Online%20Learning%20and%20Education%20WordPress%20Theme",
                "excerpt": "",
                "featuredImage": false
            }
        };
    </script>
    <script src='https://live-edumall.thememove.com/wp-content/plugins/elementor/assets/js/frontend.min.js' id='elementor-frontend-js'></script>
    <script src='https://live-edumall.thememove.com/wp-content/themes/edumall/elementor/assets/js/widgets/grid-query.js' id='edumall-grid-query-js'></script>
    <script src='https://live-edumall.thememove.com/wp-content/themes/edumall/elementor/assets/js/widgets/widget-course-tabs.js' id='edumall-widget-course-tabs-js'></script>
    <script src='https://live-edumall.thememove.com/wp-content/themes/edumall/elementor/assets/js/widgets/group-widget-carousel.js' id='edumall-group-widget-carousel-js'></script>
    <script src='https://live-edumall.thememove.com/wp-content/plugins/elementor-pro/assets/lib/sticky/jquery.sticky.min.js' id='elementor-sticky-js'></script>
    <script id='elementor-pro-frontend-js-before'>
        var ElementorProFrontendConfig = {
            "ajaxurl": "https:\/\/live-edumall.thememove.com\/wp-admin\/admin-ajax.php",
            "nonce": "71c673cb47",
            "i18n": {
                "toc_no_headings_found": "No headings were found on this page."
            },
            "shareButtonsNetworks": {
                "facebook": {
                    "title": "Facebook",
                    "has_counter": true
                },
                "twitter": {
                    "title": "Twitter"
                },
                "google": {
                    "title": "Google+",
                    "has_counter": true
                },
                "linkedin": {
                    "title": "LinkedIn",
                    "has_counter": true
                },
                "pinterest": {
                    "title": "Pinterest",
                    "has_counter": true
                },
                "reddit": {
                    "title": "Reddit",
                    "has_counter": true
                },
                "vk": {
                    "title": "VK",
                    "has_counter": true
                },
                "odnoklassniki": {
                    "title": "OK",
                    "has_counter": true
                },
                "tumblr": {
                    "title": "Tumblr"
                },
                "digg": {
                    "title": "Digg"
                },
                "skype": {
                    "title": "Skype"
                },
                "stumbleupon": {
                    "title": "StumbleUpon",
                    "has_counter": true
                },
                "mix": {
                    "title": "Mix"
                },
                "telegram": {
                    "title": "Telegram"
                },
                "pocket": {
                    "title": "Pocket",
                    "has_counter": true
                },
                "xing": {
                    "title": "XING",
                    "has_counter": true
                },
                "whatsapp": {
                    "title": "WhatsApp"
                },
                "email": {
                    "title": "Email"
                },
                "print": {
                    "title": "Print"
                }
            },
            "menu_cart": {
                "cart_page_url": "https:\/\/live-edumall.thememove.com\/cart\/",
                "checkout_page_url": "https:\/\/live-edumall.thememove.com\/checkout\/"
            },
            "facebook_sdk": {
                "lang": "en_US",
                "app_id": ""
            },
            "lottie": {
                "defaultAnimationUrl": "https:\/\/live-edumall.thememove.com\/wp-content\/plugins\/elementor-pro\/modules\/lottie\/assets\/animations\/default.json"
            }
        };
    </script>
    <script src='https://live-edumall.thememove.com/wp-content/plugins/elementor-pro/assets/js/frontend.min.js' id='elementor-pro-frontend-js'></script>