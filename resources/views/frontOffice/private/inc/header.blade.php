

<div class="main-header">
    <div class="navbar-header ml-3">
        <div class="navbar-brand">
            <a href="{{ route('showHome') }}">
                <span class="or">You</span><span class="bl">Fors</span></a>
        </div>
    </div>

    <div class="menu-toggle">
        <div></div>
        <div></div>
        <div></div>
    </div>


    <div class="d-flex align-items-center">
    </div>

    <div style="margin: auto"></div>

    <div class="header-part-right">
        <!-- Full screen toggle -->
        <i class="i-Full-Screen header-icon d-none d-sm-inline-block" data-fullscreen></i>
        <!-- Grid menu Dropdown -->

        <!-- User avatar dropdown -->
    <a href="{{route('showHome')}}" class="  " > <i class="i-Home-4 header-icon d-none d-sm-inline-block"></i> </a>
        <div class="dropdown">
            <div  class="user col align-self-end">
                <img @if(Auth::user()->photo) src="{{ asset( Auth::user()->photo) }}" @else src="{{ asset('img/unknown.png') }}" @endif id="userDropdown" alt="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                    <div class="dropdown-header">
                        <i class="i-Lock-User mr-1"></i> {{ Auth::user()->name }}
                    </div>
                    <a class="dropdown-item" href="{{ route('showHome') }}">{{ trans('lang.back_home') }}</a>
                    <a class="dropdown-item" href="{{ route('handleLogout') }}">{{ trans('lang.logout') }}</a>
                </div>
            </div>
        </div>
    </div>

</div>
