var AplCss, targetSection, tar, classname;
! function($) {
    "use strict";
    jQuery(window).on("load", function() {
        jQuery("#status").fadeOut(), jQuery("#preloader").delay(200).fadeOut("slow")
    }), jQuery(document).ready(function($) {
        var $this = $(window),
            window_height = window.innerHeight;
        $(".lernen_loadmore").slice(0, 3).show(), $("#loadMore").on("click", function(e) {
            e.preventDefault(), $(".lernen_loadmore:hidden").slice(0, 5).slideDown(), 0 == $(".lernen_loadmore:hidden").length && $("#load").fadeOut("slow")
        }), $(".timer").appear(function() {
            $(this).countTo()
        }), $("div.lernen_img_click").on("click", function() {
            if (!$(this).hasClass("active")) {
                $("div.lernen_services_slider_imgs img").removeClass("active");
                var e = $("div.lernen_services_slider_imgs .img_" + $(this).attr("id")),
                    t = e[0].outerHTML;
                e.remove(), $("div.lernen_services_slider_imgs").prepend(t), $("div.lernen_services_slider_imgs img:first").addClass("active"), $("div.lernen_services_slider_box .lernen_img_click").removeClass("active"), $(this).addClass("active")
            }
        });
        // Script for Header Background - Height 100% //
        if ($(document).width() >= 769) {
            $(window).on("resize", function () {
                if ($(window).width() < 769) {
                    $('.header-content-full').height("auto");  // Mobile version size "auto"
                }
                else {
                    var height = $(window).height();        //Get the height of the browser window
                    $('.header-content-full').height(height - 200);  //Resize the videocontainer div, with a size of 64 - page height.
                }
            }).resize();
        } else {
        }
        // End Script for Header Background - Height 100% //
        // Scroll to top button
        jQuery(document).ready(function($) {


            // Mobile Menu Show Hide Submenu
            $('#header .navbar-default .navbar-nav > li.menu-item-has-children ul').after('<div class="nav__expand"><i class="fas fa-chevron-down"></i></div>');

            $('#header .navbar-default .navbar-nav li .nav__expand').toggle( function() {
                jQuery(this).prev('ul').show(500);
                $(this).find('.fas.fa-chevron-down').removeClass('fa-chevron-down').addClass('fa-chevron-up');


            }, function() {
                $(this).find('.fas.fa-chevron-up').removeClass('fa-chevron-up').addClass('fa-chevron-down');
                jQuery(this).prev('ul').hide('show-sub-menu');
            });
            // End Submenu mobile




            // browser window scroll (in pixels) after which the "back to top" link is shown
            var offset = 300,
                //browser window scroll (in pixels) after which the "back to top" link opacity is reduced
                offset_opacity = 1200,
                //duration of the top scrolling animation (in ms)
                scroll_top_duration = 700,
                //grab the "back to top" link
                $back_to_top = $('.cd-top');

            //hide or show the "back to top" link
            $(window).scroll(function() {
                ($(this).scrollTop() > offset) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
                if ($(this).scrollTop() > offset_opacity) {
                    $back_to_top.addClass('cd-fade-out');
                }
            });

            //smooth scroll to top
            $back_to_top.on('click', function(event) {
                event.preventDefault();
                $('body,html').animate({
                        scrollTop: 0,
                    }, scroll_top_duration
                );
            });

        });
        // End Scroll to top
        // Navigation menu scrollspy to anchor section //
        $('body').scrollspy({
            target: '#navigation .navbar-collapse',
            offset: parseInt($('#navigation').height(), 0)
        });
        // End navigation menu scrollspy to anchor section //
        // sticky-menu on scroll
        $(window).on('scroll', function () {
            var scroll = $(window).scrollTop();
            if (scroll < 245) {
                $("#header").removeClass("sticky-menu");
            } else {
                $("#header").addClass("sticky-menu");
            }
        });

            // jQuery tooltips //
            $('.btn-tooltip').tooltip();
        $('.btn-popover').popover();
        // End jQuery tooltips //

        $('.owl-carousel').owlCarousel({
            loop:true,
            margin:0,
            nav:false,
            touchDrag:true,
            mouseDrag:false,
            autoplay:true,
            autoplayTimeout:5000,
            smartSpeed: 1000,
            autoplayHoverPause:true,
            responsive:{
                0:{
                    items:1
                }
            }
        });
        // Team Slider Slick
        $('.carousel-slider.gallery-slider').slick({
            arrows: false,
            dots: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 5000,
            draggable: true,

            responsive: [
                {
                    breakpoint: 990,
                    settings: {
                        slidesToShow: 1,
                        draggable: true
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 1,
                        draggable: true
                    }

                }
            ]
        });
        // End Team Slider Slick
        // Sponsor Slider Slick
        $('.carousel-slider.sponsor-slider').slick({
            arrows: false,
            dots: false,
            slidesToShow: 6,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 2500,
            draggable: true,
            responsive: [
                {
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 6,
                        draggable: true
                    }
                },
                {
                    breakpoint: 990,
                    settings: {
                        slidesToShow: 4,
                        draggable: true
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 3,
                        draggable: true
                    }

                }
            ]
        });
        // End Sponsor Slider Slick
        // Students Review Slider Slick
        $('.carousel-slider.general-slider').each(function() {
            $(this).slick({
                arrows: false,
                dots: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 5000,
                draggable: true,
                responsive: [{
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 1,
                        draggable: true
                    }
                }]
            });
        });
        // End Students Review Slider Slick
        // Preview images popup gallery with Fancybox //
        $('.fancybox').fancybox({
            loop: false
        });
        // End Preview images popup gallery with Fancybox //
        // Counter animation //
        $('.themeioan_counter > h4').counterUp ({
            delay: 10,
            time: 3000
        });
        // End Counter animation //
        // Navigation Burger animation //
        $('.burger-icon').on('click touchstart', function(e) {
            $(this).toggleClass('change');
            $("#navbarCollapse").slideToggle();
            e.preventDefault();
        });
        // END Navigation Burger animation //
        function checkRequire(formId, targetResp) {
            targetResp.html("");
            var email = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/,
                url = /(http|ftp|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?/,
                image = /\.(jpe?g|gif|png|PNG|JPE?G)$/,
                mobile = /^[\s()+-]*([0-9][\s()+-]*){6,20}$/,
                facebook = /^(https?:\/\/)?(www\.)?facebook.com\/[a-zA-Z0-9(\.\?)?]/,
                twitter = /^(https?:\/\/)?(www\.)?twitter.com\/[a-zA-Z0-9(\.\?)?]/,
                google_plus = /^(https?:\/\/)?(www\.)?plus.google.com\/[a-zA-Z0-9(\.\?)?]/,
                check = 0;
            $("#er_msg").remove();
            var target = $("object" == typeof formId ? formId : "#" + formId);
            return target.find("input , textarea , select").each(function() {
                if ($(this).hasClass("require")) {
                    if ("" == $(this).val().trim()) return check = 1, $(this).focus(), targetResp.html("You missed out some fields."), $(this).addClass("error"), !1;
                    $(this).removeClass("error")
                }
                if ("" != $(this).val().trim()) {
                    var valid = $(this).attr("data-valid");
                    if (void 0 !== valid) {
                        if (!eval(valid).test($(this).val().trim())) return $(this).addClass("error"), $(this).focus(), check = 1, targetResp.html($(this).attr("data-error")), !1;
                        $(this).removeClass("error")
                    }
                }
            }), check
        }
    }), jQuery(".related.products .owl-carousel").owlCarousel({
        delay: 9e3,
        loop: !0,
        margin: 10,
        nav: !1,
        dots: !0,
        autoplay: !0,
        autoplayHoverPause: !0,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3
            },
            1e3: {
                items: 4
            }
        }
    }), jQuery(".self_service_detail").addClass("animated zoomIn"), jQuery(".flex-control-thumbs li img").on("click", function() {
        var e = jQuery(this).attr("src");
        jQuery(".sl_product_img_target").attr("src", e)
    })

}();
var timing = 400;
jQuery("#header ul").on("click", "li", function() {
    jQuery("#header ul li.active").removeClass("active"), jQuery(this).addClass("active")
}), jQuery(".header_tabs li a.header_one").on("click", function() {
    jQuery("#header").addClass("header_one"), jQuery("#header").removeClass("header_two , header_three")
}), jQuery(".header_tabs li a.header_two").on("click", function() {
    jQuery("#header").addClass("header_two"), jQuery("#header").removeClass("header_one , header_three")
}), jQuery(".header_tabs li a.header_three").on("click", function() {
    jQuery("#header").addClass("header_three"), jQuery("#header").removeClass("header_one , header_two")
});
