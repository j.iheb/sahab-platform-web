<?php

namespace App\Modules\General\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class WebController extends Controller
{



    public function showHome()
    {
        return view('General::frontOffice.home'); 
    }


    public function showManagerHome()
    {
        
        return view('General::backOffice.home');
    }

    public function showCommunities()
    {
        return view('General::frontOffice.communities'); 
    }

    public function showProjects()
    {
        return view('General::frontOffice.projects'); 
    }


    public function showNotifications()
    {
        return view('General::frontOffice.notifications'); 
    }
}
