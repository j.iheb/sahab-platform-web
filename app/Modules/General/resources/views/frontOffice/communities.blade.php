@extends('frontOffice.layout')

@section('head')
@include('frontOffice.inc.head',
['title' => ' - Administration',
'description' => 'Espace Administratif - '
])
@endsection

@section('header')
@include('frontOffice.inc.header')
@endsection






@section('content')


<!-- Page Content -->
<div id="body-container">

    <div class="p-4 px-md-5">
        <div class="row">
            <div class="col-md-12 text-center">
                <h3>المجتمعات</h3>
                <div class="scroll-wrapper">
                    <a class="text-decoration-none border-primary rounded-circle border scroll-item" href="#" data-toggle="tooltip" data-placement="left" title="مجتمع دورة تصميم صفحات الإنترنت CSS HTML" style="height: 80px; width:80px;
                                   background-size: cover;
                                   background-position: center;
                                   background-image: url('https://cdn.flexcourses.com/app/public/courses/images/2409/conversions/html_css-thumb.jpg')">
                    </a>
                    <a class="text-decoration-none border-primary rounded-circle border scroll-item" href="#" data-toggle="tooltip" data-placement="left" title="مجتمع دورة بناء صفحات الإنترنت المرنة باستخدام Bootstrap V4" style="height: 80px; width:80px;
                                   background-size: cover;
                                   background-position: center;
                                   background-image: url('https://cdn.flexcourses.com/app/public/courses/images/2422/conversions/bootstrap-%D9%82%D8%A7%D9%84%D8%A8-thumb.jpg')">
                    </a>
                    <a class="text-decoration-none border-primary rounded-circle border scroll-item" href="#" data-toggle="tooltip" data-placement="left" title="مجتمع دورة بناء متجر إلكتروني مع Wordpress" style="height: 80px; width:80px;
                                   background-size: cover;
                                   background-position: center;
                                   background-image: url('https://cdn.flexcourses.com/app/public/courses/images/2431/conversions/%D9%82%D8%A7%D9%84%D8%A8-WOO-thumb.jpg')">
                    </a>
                    <a class="text-decoration-none border-primary rounded-circle border scroll-item" href="#" data-toggle="tooltip" data-placement="left" title="مجتمع دورة jQuery جافا سكربت من الصفر للاحتراف" style="height: 80px; width:80px;
                                   background-size: cover;
                                   background-position: center;
                                   background-image: url('https://cdn.flexcourses.com/app/public/courses/images/2406/conversions/jquery-thumb.jpg')">
                    </a>
                    <a class="text-decoration-none border-primary rounded-circle border scroll-item" href="#" data-toggle="tooltip" data-placement="left" title="مجتمع دورة برمجة الويب باستخدام لارافيل Laravel" style="height: 80px; width:80px;
                                   background-size: cover;
                                   background-position: center;
                                   background-image: url('https://cdn.flexcourses.com/app/public/courses/images/2408/conversions/laravel-thumb.jpg')">
                    </a>
 
                </div>
            </div>
            <div class="col-md-12">
                <nav>
                    <ul class="pagination">

                        <li class="page-item disabled" aria-disabled="true" aria-label="&laquo; Previous">
                            <span class="page-link" aria-hidden="true">&lsaquo;</span>
                        </li>





                        <li class="page-item active" aria-current="page"><span class="page-link">1</span></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item"><a class="page-link" href="#">4</a></li>
                        <li class="page-item"><a class="page-link" href="#">5</a></li>
                        <li class="page-item"><a class="page-link" href="#">6</a></li>


                        <li class="page-item">
                            <a class="page-link" href="#" rel="next" aria-label="Next &raquo;">&rsaquo;</a>
                        </li>
                    </ul>
                </nav>

                <div class="row">

                    <div class="col-lg-4 col-md-6 col-xl-3 p-3">
                        <div class="card mb-3 community community-13" style="height: 100%">
                            <a href="#">
                                <h5 class="card-header bg-white text-truncate">
                                    سؤال بخصوص تخزين الصور
                                </h5>
                            </a>


                            <div class="card-body text-secondary">
                                &nbsp;&nbsp;يظهر لي هالخطا لمن ابي اخزن صوره وهذا الكود تبع الفورم&nbsp; &lt;form action="#" method="post" enctype="multipart/form-data"&gt;
                                <a class="text-secondary" href="#">
                                    ...المزيد
                                </a>
                            </div>
                            <div class="card-footer bg-white">
                                <div class="row align-items-center">
                                    <div class="col-3 px-0 text-center">
                                        <a href="#">
                                            <div style="background-image: url('https://cdn.flexcourses.com/app/public/courses/images/2408/conversions/laravel-thumb.jpg');" class="rounded-circle img-thumbnail img-thumbnail-small border-primary d-inline-block">
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-9 pr-0">
                                        <div class="small text-truncate">
                                            <a class="text-secondary" href="#">مجتمع دورة برمجة الويب باستخدام لارافيل Laravel</a>
                                        </div>
                                        <div class="small">
                                            <span>منذ شهرين</span>
                                            <a class="text-dark float-left" href="#"><span class="__cf_email__" data-cfemail="f899829782cfc9ccb8">email@gmail.com</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-xl-3 p-3">
                        <div class="card mb-3 community community-11" style="height: 100%">
                            <a href="#">
                                <h5 class="card-header bg-white text-truncate">
                                    كيف اربط شركات الشحن بمتجري
                                </h5>
                            </a>


                            <div class="card-body text-secondary">
                                واجهة عدة مشاكل بعد ما انشأت متجري&nbsp;1- ما عرفت كيف اربط بوابات الدفع الالكترونية بالمتجر (مدى-ماستركارد\فيزا-ابل باي)2-كيف اربط شركات الشحن بالمتجر3- كيف يكون الدعم الفني والصيانة للموقع (هل يتم عن طريق شركات الاستضافه)4- اخيرا هل من الطبيعي اني اقوم بكل هذي الامور لوحدي ام احتاج الى فريق لتقسيم العمل
                                <a class="text-secondary" href="#">
                                    ...المزيد
                                </a>
                            </div>
                            <div class="card-footer bg-white">
                                <div class="row align-items-center">
                                    <div class="col-3 px-0 text-center">
                                        <a href="#">
                                            <div style="background-image: url('https://cdn.flexcourses.com/app/public/courses/images/2431/conversions/%D9%82%D8%A7%D9%84%D8%A8-WOO-thumb.jpg');" class="rounded-circle img-thumbnail img-thumbnail-small border-primary d-inline-block">
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-9 pr-0">
                                        <div class="small text-truncate">
                                            <a class="text-secondary" href="#">مجتمع دورة بناء متجر إلكتروني مع Wordpress</a>
                                        </div>
                                        <div class="small">
                                            <span>منذ شهرين</span>
                                            <a class="text-dark float-left" href="#"><span class="__cf_email__" data-cfemail="5801392b2b3d2a1c3735393618">email@gmail.com</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-xl-3 p-3">
                        <div class="card mb-3 community community-20" style="height: 100%">
                            <a href="#">
                                <h5 class="card-header bg-white text-truncate">
                                    التعّديل على كل عناصر الكلاس
                                </h5>
                            </a>


                            <div class="card-body text-secondary">
                                السلام عليكم.انا ابي اعدّل على بعض خصائص الكلاس, لكن المشكلة هي ان بعض الأوامر ما تشتغل فيها.
                            </div>
                            <div class="card-footer bg-white">
                                <div class="row align-items-center">
                                    <div class="col-3 px-0 text-center">
                                        <a href="#">
                                            <div style="background-image: url('https://cdn.flexcourses.com/app/public/courses/images/2420/conversions/js-%D9%82%D8%A7%D9%84%D8%A8-thumb.jpg');" class="rounded-circle img-thumbnail img-thumbnail-small border-primary d-inline-block">
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-9 pr-0">
                                        <div class="small text-truncate">
                                            <a class="text-secondary" href="#">مجتمع دورة البرمجة باستخدام جافا سكريبت Javascript</a>
                                        </div>
                                        <div class="small">
                                            <span>منذ 3 أشهر</span>
                                            <a class="text-dark float-left" href="#"><span class="__cf_email__" data-cfemail="d3b7b2a1b8b2b4b6bda7e293">email@gmail.com</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-xl-3 p-3">
                        <div class="card mb-3 community community-13" style="height: 100%">
                            <a href="#">
                                <h5 class="card-header bg-white text-truncate">
                                    اختصار تنضيف الكاش
                                </h5>
                            </a>


                            <div class="card-body text-secondary">
                                السلام عليكم ورحمة الله وبركاته&nbsp;هذا كود يحذف لك كل ملفات الكاش وملفات الـ logs والجلسات او&nbsp;Sessions عن طريق انشاء امر في Artisan(تنبيه الكود هذا يحذف اشياء كثير ومنها الجلسات او&nbsp;Sessions)اولاً الصق الكود هذا في ملف&nbsp;routes/console.phpArtisan::command('clear-all', function() {

                                $this-&gt;comment('P
                                <a class="text-secondary" href="#">
                                    ...المزيد
                                </a>
                            </div>
                            <div class="card-footer bg-white">
                                <div class="row align-items-center">
                                    <div class="col-3 px-0 text-center">
                                        <a href="#">
                                            <div style="background-image: url('https://cdn.flexcourses.com/app/public/courses/images/2408/conversions/laravel-thumb.jpg');" class="rounded-circle img-thumbnail img-thumbnail-small border-primary d-inline-block">
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-9 pr-0">
                                        <div class="small text-truncate">
                                            <a class="text-secondary" href="#">مجتمع دورة برمجة الويب باستخدام لارافيل Laravel</a>
                                        </div>
                                        <div class="small">
                                            <span>منذ 3 أشهر</span>
                                            <a class="text-dark float-left" href="#"><span class="__cf_email__" data-cfemail="afd6dac0dccec99f9eef">email@gmail.com</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <nav>
                    <ul class="pagination">

                        <li class="page-item disabled" aria-disabled="true" aria-label="&laquo; Previous">
                            <span class="page-link" aria-hidden="true">&lsaquo;</span>
                        </li>





                        <li class="page-item active" aria-current="page"><span class="page-link">1</span></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item"><a class="page-link" href="#">4</a></li>
                        <li class="page-item"><a class="page-link" href="#">5</a></li>
                        <li class="page-item"><a class="page-link" href="#">6</a></li>


                        <li class="page-item">
                            <a class="page-link" href="#" rel="next" aria-label="Next &raquo;">&rsaquo;</a>
                        </li>
                    </ul>
                </nav>

            </div>
        </div>
    </div>


</div>
<!-- /.container -->
@endsection