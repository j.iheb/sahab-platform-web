@extends('frontOffice.layout')

@section('head')
@include('frontOffice.inc.head',
['title' => ' - Administration',
'description' => 'Espace Administratif - '
])
@endsection




@section('content')


<div id="page" class="site">
    <div class="content-wrapper">

        @section('header')
        @include('frontOffice.inc.header')
        @endsection


        <div id="page-content" class="page-content">
            <div class="container">
                <div class="row">


                    <div id="page-main-content" class="page-main-content">

                        <div class="rich-snippet display-none">
                            <h1 class="entry-title">Course Hub</h1> <span class="published">August 27, 2020</span>
                            <span class="updated" data-time="2020-11-12 2:29">2020-11-12 2:29</span>
                        </div>

                        <article id="post-619" class="post-619 page type-page status-publish hentry pmpro-has-access post-no-thumbnail">
                            <h2 class="screen-reader-text">Course Hub</h2>
                            <div data-elementor-type="wp-page" data-elementor-id="619" class="elementor elementor-619" data-elementor-settings="[]">
                                <div class="elementor-inner">
                                    <div class="elementor-section-wrap">
                                      <br> <br> <br>
                                       

                                      <section class="elementor-section elementor-top-section elementor-element elementor-element-1431b8e elementor-section-boxed elementor-section-gap-beside-yes elementor-section-height-default elementor-section-height-default elementor-section-column-vertical-align-stretch" data-id="1431b8e" data-element_type="section">
                                                <div class="elementor-container elementor-column-gap-extended">
                                                    <div class="elementor-row">
                                                        <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-30d4890" data-id="30d4890" data-element_type="column">
                                                            <div class="elementor-column-wrap elementor-element-populated">
                                                                <div class="elementor-widget-wrap">
                                                                    <section class="elementor-section elementor-inner-section elementor-element elementor-element-53a2d91 elementor-section-gap-beside-no elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section-column-vertical-align-stretch" data-id="53a2d91" data-element_type="section">
                                                                        <div class="elementor-container elementor-column-gap-extended">
                                                                            <div class="elementor-row">
                                                                                <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-a10648b" data-id="a10648b" data-element_type="column">
                                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                                        <div class="elementor-widget-wrap">
                                                                                            <div class="elementor-element elementor-element-b21aed8 edumall-modern-heading-style-01 elementor-invisible elementor-widget elementor-widget-tm-heading" data-id="b21aed8" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;edumallFadeInUp&quot;}" data-widget_type="tm-heading.default">
                                                                                                <div class="elementor-widget-container">
                                                                                                    <div class="tm-modern-heading">

                                                                                                        <div class="heading-primary-wrap">
                                                                                                            <h3 class="heading-primary elementor-heading-title">What's Happening In <mark>EduMall?</mark></h3>
                                                                                                        </div>


                                                                                                        <div class="heading-description-wrap">
                                                                                                            <div class="heading-description">
                                                                                                                <p>Show all Featured courses and News for your students</p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="elementor-element elementor-element-de2a74a edumall-rich-carousel-style-01 bullets-v-align-below elementor-invisible elementor-widget elementor-widget-tm-rich-carousel" data-id="de2a74a" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;edumallFadeInUp&quot;}" data-widget_type="tm-rich-carousel.default">
                                                                                                <div class="elementor-widget-container">
                                                                                                    <div class="edumall-rich-carousel">

                                                                                                        <div class="tm-swiper tm-slider-widget pagination-style-01 bullets-horizontal bullets-h-align-center bullets-v-align-below" data-lg-items="3" data-md-items="2" data-sm-items="1" data-lg-gutter="30" data-md-gutter="" data-sm-gutter="" data-pagination-aligned-by="slider" data-pagination="1" data-loop="1" data-simulate-touch="1" data-speed="1000" data-effect="slide">
                                                                                                            <div class="swiper-inner">


                                                                                                                <div class="swiper-container">
                                                                                                                    <div class="swiper-wrapper">
                                                                                                                        <div class="swiper-slide elementor-repeater-item-797f3c1">
                                                                                                                            <a class="edumall-box slide-wrapper link-secret" href="#">
                                                                                                                                <div class="slide-image edumall-image">
                                                                                                                                    <img src="https://live-edumall.thememove.com/wp-content/uploads/2020/10/home-language-academic-banner-01.jpg" alt="home-language-academic-banner-01" />
                                                                                                                                </div>
                                                                                                                            </a>
                                                                                                                        </div>
                                                                                                                        <div class="swiper-slide elementor-repeater-item-b298282">
                                                                                                                            <a class="edumall-box slide-wrapper link-secret" href="#">
                                                                                                                                <div class="slide-image edumall-image">
                                                                                                                                    <img src="https://live-edumall.thememove.com/wp-content/uploads/2020/10/home-language-academic-banner-02.jpg" alt="home-language-academic-banner-02" />
                                                                                                                                </div>
                                                                                                                            </a>
                                                                                                                        </div>
                                                                                                                        <div class="swiper-slide elementor-repeater-item-abc3c6f">
                                                                                                                            <a class="edumall-box slide-wrapper link-secret" href="#">
                                                                                                                                <div class="slide-image edumall-image">
                                                                                                                                    <img src="https://live-edumall.thememove.com/wp-content/uploads/2020/10/home-language-academic-banner-03.jpg" alt="home-language-academic-banner-03" />
                                                                                                                                </div>
                                                                                                                            </a>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>


                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </section>
                                                                    <section class="elementor-section elementor-inner-section elementor-element elementor-element-0530928 elementor-section-gap-beside-no elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section-column-vertical-align-stretch" data-id="0530928" data-element_type="section">
                                                                        <div class="elementor-container elementor-column-gap-extended">
                                                                            <div class="elementor-row">
                                                                                <div class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-40bc1d4" data-id="40bc1d4" data-element_type="column">
                                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                                        <div class="elementor-widget-wrap">
                                                                                            <div class="elementor-element elementor-element-fa8df72 elementor-invisible elementor-widget elementor-widget-tm-image-box" data-id="fa8df72" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;edumallFadeInUp&quot;}" data-widget_type="tm-image-box.default">
                                                                                                <div class="elementor-widget-container">
                                                                                                    <a class="tm-image-box edumall-box style-01 image-position-top content-alignment- link-secret" href="#">
                                                                                                        <div class="content-wrap">

                                                                                                            <div class="image-wrap">
                                                                                                                <div class="edumall-image image">
                                                                                                                    <img src="https://live-edumall.thememove.com/wp-content/uploads/2020/10/home-language-academic-image-box-01.png" alt="home-language-academic-image-box-01" />
                                                                                                                </div>

                                                                                                            </div>

                                                                                                            <div class="box-caption-wrap">
                                                                                                                <div class="box-caption">
                                                                                                                    <h3 class="title">15 Languages from Beginner to Expert</h3>

                                                                                                                </div>
                                                                                                            </div>

                                                                                                        </div>
                                                                                                    </a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-e327a20" data-id="e327a20" data-element_type="column">
                                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                                        <div class="elementor-widget-wrap">
                                                                                            <div class="elementor-element elementor-element-9ab02ce elementor-invisible elementor-widget elementor-widget-tm-image-box" data-id="9ab02ce" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;edumallFadeInUp&quot;}" data-widget_type="tm-image-box.default">
                                                                                                <div class="elementor-widget-container">
                                                                                                    <a class="tm-image-box edumall-box style-01 image-position-top content-alignment- link-secret" href="#">
                                                                                                        <div class="content-wrap">

                                                                                                            <div class="image-wrap">
                                                                                                                <div class="edumall-image image">
                                                                                                                    <img src="https://live-edumall.thememove.com/wp-content/uploads/2020/10/home-language-academic-image-box-02.png" alt="home-language-academic-image-box-02" />
                                                                                                                </div>

                                                                                                            </div>

                                                                                                            <div class="box-caption-wrap">
                                                                                                                <div class="box-caption">
                                                                                                                    <h3 class="title">One-to-One or Group Language Programs</h3>

                                                                                                                </div>
                                                                                                            </div>

                                                                                                        </div>
                                                                                                    </a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-b65b6b4" data-id="b65b6b4" data-element_type="column">
                                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                                        <div class="elementor-widget-wrap">
                                                                                            <div class="elementor-element elementor-element-f7f520d elementor-invisible elementor-widget elementor-widget-tm-image-box" data-id="f7f520d" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;edumallFadeInUp&quot;}" data-widget_type="tm-image-box.default">
                                                                                                <div class="elementor-widget-container">
                                                                                                    <a class="tm-image-box edumall-box style-01 image-position-top content-alignment- link-secret" href="#">
                                                                                                        <div class="content-wrap">

                                                                                                            <div class="image-wrap">
                                                                                                                <div class="edumall-image image">
                                                                                                                    <img src="https://live-edumall.thememove.com/wp-content/uploads/2020/10/home-language-academic-image-box-03.png" alt="home-language-academic-image-box-03" />
                                                                                                                </div>

                                                                                                            </div>

                                                                                                            <div class="box-caption-wrap">
                                                                                                                <div class="box-caption">
                                                                                                                    <h3 class="title">Instructed by Language Experts</h3>

                                                                                                                </div>
                                                                                                            </div>

                                                                                                        </div>
                                                                                                    </a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-bf57dd1" data-id="bf57dd1" data-element_type="column">
                                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                                        <div class="elementor-widget-wrap">
                                                                                            <div class="elementor-element elementor-element-577286e elementor-invisible elementor-widget elementor-widget-tm-image-box" data-id="577286e" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;edumallFadeInUp&quot;}" data-widget_type="tm-image-box.default">
                                                                                                <div class="elementor-widget-container">
                                                                                                    <a class="tm-image-box edumall-box style-01 image-position-top content-alignment- link-secret" href="#">
                                                                                                        <div class="content-wrap">

                                                                                                            <div class="image-wrap">
                                                                                                                <div class="edumall-image image">
                                                                                                                    <img src="https://live-edumall.thememove.com/wp-content/uploads/2020/10/home-language-academic-image-box-04.png" alt="home-language-academic-image-box-04" />
                                                                                                                </div>

                                                                                                            </div>

                                                                                                            <div class="box-caption-wrap">
                                                                                                                <div class="box-caption">
                                                                                                                    <h3 class="title">A Residential Campus Community</h3>

                                                                                                                </div>
                                                                                                            </div>

                                                                                                        </div>
                                                                                                    </a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </section>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                        
                                       

                                    <br> <br> <br>
                                     


                                    <section class="elementor-section elementor-top-section elementor-element elementor-element-68d0918 elementor-section-full_width elementor-section-gap-beside-yes elementor-section-height-default elementor-section-height-default elementor-section-column-vertical-align-stretch" data-id="68d0918" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                                                <div class="elementor-container elementor-column-gap-extended">
                                                    <div class="elementor-row">
                                                        <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-5aaeea6" data-id="5aaeea6" data-element_type="column">
                                                            <div class="elementor-column-wrap elementor-element-populated">
                                                                <div class="elementor-widget-wrap">
                                                                    <div class="elementor-element elementor-element-93958f3 edumall-shape-border-circle elementor-widget__width-auto elementor-absolute elementor-hidden-tablet elementor-hidden-phone elementor-widget elementor-widget-tm-shapes" data-id="93958f3" data-element_type="widget" data-settings="{&quot;motion_fx_motion_fx_mouse&quot;:&quot;yes&quot;,&quot;motion_fx_mouseTrack_effect&quot;:&quot;yes&quot;,&quot;motion_fx_mouseTrack_direction&quot;:&quot;negative&quot;,&quot;_position&quot;:&quot;absolute&quot;,&quot;motion_fx_mouseTrack_speed&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:1,&quot;sizes&quot;:[]}}" data-widget_type="tm-shapes.default">
                                                                        <div class="elementor-widget-container">
                                                                            <div class="edumall-shape edumall-shape-1-1">
                                                                                <div class="shape"></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="elementor-element elementor-element-302036e edumall-shape-border-circle elementor-widget__width-auto elementor-absolute elementor-widget elementor-widget-tm-shapes" data-id="302036e" data-element_type="widget" data-settings="{&quot;motion_fx_motion_fx_mouse&quot;:&quot;yes&quot;,&quot;motion_fx_mouseTrack_effect&quot;:&quot;yes&quot;,&quot;_position&quot;:&quot;absolute&quot;,&quot;motion_fx_mouseTrack_speed&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:1,&quot;sizes&quot;:[]}}" data-widget_type="tm-shapes.default">
                                                                        <div class="elementor-widget-container">
                                                                            <div class="edumall-shape edumall-shape-1-1">
                                                                                <div class="shape"></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="elementor-element elementor-element-d68ad1d elementor-widget__width-auto elementor-absolute elementor-widget-tablet__width-initial elementor-widget elementor-widget-image" data-id="d68ad1d" data-element_type="widget" data-settings="{&quot;motion_fx_motion_fx_mouse&quot;:&quot;yes&quot;,&quot;motion_fx_mouseTrack_effect&quot;:&quot;yes&quot;,&quot;motion_fx_mouseTrack_direction&quot;:&quot;negative&quot;,&quot;_position&quot;:&quot;absolute&quot;,&quot;motion_fx_mouseTrack_speed&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:1,&quot;sizes&quot;:[]}}" data-widget_type="image.default">
                                                                        <div class="elementor-widget-container">
                                                                            <div class="elementor-image">
                                                                                <img width="179" height="178" src="https://live-edumall.thememove.com/wp-content/uploads/2020/08/edumall-shape-01.png" class="attachment-full size-full" alt="" loading="lazy" srcset="https://live-edumall.thememove.com/wp-content/uploads/2020/08/edumall-shape-01.png 179w, https://live-edumall.thememove.com/wp-content/uploads/2020/08/edumall-shape-01-150x150.png 150w, https://live-edumall.thememove.com/wp-content/uploads/2020/08/edumall-shape-01-100x100.png 100w, https://live-edumall.thememove.com/wp-content/uploads/2020/08/edumall-shape-01-96x96.png 96w" sizes="(max-width: 179px) 100vw, 179px" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="elementor-element elementor-element-28265b1 edumall-shape-border-circle elementor-widget__width-auto elementor-absolute elementor-hidden-tablet elementor-hidden-phone elementor-widget elementor-widget-tm-shapes" data-id="28265b1" data-element_type="widget" data-settings="{&quot;motion_fx_motion_fx_mouse&quot;:&quot;yes&quot;,&quot;motion_fx_mouseTrack_effect&quot;:&quot;yes&quot;,&quot;motion_fx_mouseTrack_direction&quot;:&quot;negative&quot;,&quot;_position&quot;:&quot;absolute&quot;,&quot;motion_fx_mouseTrack_speed&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:1,&quot;sizes&quot;:[]}}" data-widget_type="tm-shapes.default">
                                                                        <div class="elementor-widget-container">
                                                                            <div class="edumall-shape edumall-shape-1-1">
                                                                                <div class="shape"></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="elementor-element elementor-element-16d0d21 edumall-shape-border-circle elementor-widget__width-auto elementor-absolute elementor-widget elementor-widget-tm-shapes" data-id="16d0d21" data-element_type="widget" data-settings="{&quot;motion_fx_motion_fx_mouse&quot;:&quot;yes&quot;,&quot;motion_fx_mouseTrack_effect&quot;:&quot;yes&quot;,&quot;_position&quot;:&quot;absolute&quot;,&quot;motion_fx_mouseTrack_speed&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:1,&quot;sizes&quot;:[]}}" data-widget_type="tm-shapes.default">
                                                                        <div class="elementor-widget-container">
                                                                            <div class="edumall-shape edumall-shape-1-1">
                                                                                <div class="shape"></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <section class="elementor-section elementor-inner-section elementor-element elementor-element-12220bf elementor-section-boxed elementor-section-gap-beside-yes elementor-section-height-default elementor-section-height-default elementor-section-column-vertical-align-stretch" data-id="12220bf" data-element_type="section">
                                                                        <div class="elementor-container elementor-column-gap-extended">
                                                                            <div class="elementor-row">
                                                                                <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-35c5960" data-id="35c5960" data-element_type="column">
                                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                                        <div class="elementor-widget-wrap">
                                                                                            <div class="elementor-element elementor-element-4b34bf9 edumall-modern-heading-style-01 elementor-invisible elementor-widget elementor-widget-tm-heading" data-id="4b34bf9" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;edumallFadeInUp&quot;}" data-widget_type="tm-heading.default">
                                                                                                <div class="elementor-widget-container">
                                                                                                    <div class="tm-modern-heading">

                                                                                                        <div class="heading-primary-wrap">
                                                                                                            <h3 class="heading-primary elementor-heading-title"><mark>Language</mark> Courses</h3>
                                                                                                        </div>


                                                                                                        <div class="heading-description-wrap">
                                                                                                            <div class="heading-description">
                                                                                                                Learning new languages, connect to the world and become a global citizen with EduMall </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="elementor-element elementor-element-b6b31f0 edumall-animation-zoom-in elementor-widget elementor-widget-tm-course-languages" data-id="b6b31f0" data-element_type="widget" data-widget_type="tm-course-languages.default">
                                                                                                <div class="elementor-widget-container">
                                                                                                    <div class="edumall-grid-wrapper edumall-course-languages" data-grid="{&quot;type&quot;:&quot;grid&quot;,&quot;ratio&quot;:null,&quot;columns&quot;:4,&quot;columnsTablet&quot;:2,&quot;columnsMobile&quot;:1,&quot;gutter&quot;:30}">
                                                                                                        <div class="edumall-grid lazy-grid">
                                                                                                            <div class="grid-sizer"></div>

                                                                                                            <div class="grid-item">
                                                                                                                <a href="https://live-edumall.thememove.com/course-language/english/" class="edumall-box course-language-wrapper cat-link link-secret">

                                                                                                                    <div class="course-thumb-wrap">
                                                                                                                        <div class="edumall-image image">
                                                                                                                            <img src="https://live-edumall.thememove.com/wp-content/uploads/2020/06/course-language-english-thumbnail-480x364.jpg" alt="course-language-english-thumbnail" width="480" />
                                                                                                                        </div>
                                                                                                                    </div>

                                                                                                                    <div class="course-language-caption">
                                                                                                                        <h5 class="course-language-name">
                                                                                                                            English Courses </h5>
                                                                                                                        <div class="course-language-info">
                                                                                                                            <div class="course-language-flag">
                                                                                                                                <img src="https://live-edumall.thememove.com/wp-content/uploads/2020/06/united-kingdom.svg" alt="united-kingdom" />
                                                                                                                            </div>

                                                                                                                            <div class="course-language-count">
                                                                                                                                22 programs </div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </a>
                                                                                                            </div>
                                                                                                            <div class="grid-item">
                                                                                                                <a href="https://live-edumall.thememove.com/course-language/french/" class="edumall-box course-language-wrapper cat-link link-secret">

                                                                                                                    <div class="course-thumb-wrap">
                                                                                                                        <div class="edumall-image image">
                                                                                                                            <img src="https://live-edumall.thememove.com/wp-content/uploads/2020/06/course-language-french-thumbnail-480x364.jpg" alt="course-language-french-thumbnail" width="480" />
                                                                                                                        </div>
                                                                                                                    </div>

                                                                                                                    <div class="course-language-caption">
                                                                                                                        <h5 class="course-language-name">
                                                                                                                            French Courses </h5>
                                                                                                                        <div class="course-language-info">
                                                                                                                            <div class="course-language-flag">
                                                                                                                                <img src="https://live-edumall.thememove.com/wp-content/uploads/2020/06/france.svg" alt="france" />
                                                                                                                            </div>

                                                                                                                            <div class="course-language-count">
                                                                                                                                7 programs </div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </a>
                                                                                                            </div>
                                                                                                            <div class="grid-item">
                                                                                                                <a href="https://live-edumall.thememove.com/course-language/german/" class="edumall-box course-language-wrapper cat-link link-secret">

                                                                                                                    <div class="course-thumb-wrap">
                                                                                                                        <div class="edumall-image image">
                                                                                                                            <img src="https://live-edumall.thememove.com/wp-content/uploads/2020/06/course-language-german-thumbnail-480x364.jpg" alt="course-language-german-thumbnail" width="480" />
                                                                                                                        </div>
                                                                                                                    </div>

                                                                                                                    <div class="course-language-caption">
                                                                                                                        <h5 class="course-language-name">
                                                                                                                            German Courses </h5>
                                                                                                                        <div class="course-language-info">
                                                                                                                            <div class="course-language-flag">
                                                                                                                                <img src="https://live-edumall.thememove.com/wp-content/uploads/2020/06/germany.svg" alt="germany" />
                                                                                                                            </div>

                                                                                                                            <div class="course-language-count">
                                                                                                                                5 programs </div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </a>
                                                                                                            </div>
                                                                                                            <div class="grid-item">
                                                                                                                <a href="https://live-edumall.thememove.com/course-language/italian/" class="edumall-box course-language-wrapper cat-link link-secret">

                                                                                                                    <div class="course-thumb-wrap">
                                                                                                                        <div class="edumall-image image">
                                                                                                                            <img src="https://live-edumall.thememove.com/wp-content/uploads/2020/06/course-language-italian-thumbnail-480x364.jpg" alt="course-language-italian-thumbnail" width="480" />
                                                                                                                        </div>
                                                                                                                    </div>

                                                                                                                    <div class="course-language-caption">
                                                                                                                        <h5 class="course-language-name">
                                                                                                                            Italian Courses </h5>
                                                                                                                        <div class="course-language-info">
                                                                                                                            <div class="course-language-flag">
                                                                                                                                <img src="https://live-edumall.thememove.com/wp-content/uploads/2020/06/italy.svg" alt="italy" />
                                                                                                                            </div>

                                                                                                                            <div class="course-language-count">
                                                                                                                                5 programs </div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </a>
                                                                                                            </div>
                                                                                                            <div class="grid-item">
                                                                                                                <a href="https://live-edumall.thememove.com/course-language/japanese/" class="edumall-box course-language-wrapper cat-link link-secret">

                                                                                                                    <div class="course-thumb-wrap">
                                                                                                                        <div class="edumall-image image">
                                                                                                                            <img src="https://live-edumall.thememove.com/wp-content/uploads/2020/06/course-language-japanese-thumbnail-480x364.jpg" alt="course-language-japanese-thumbnail" width="480" />
                                                                                                                        </div>
                                                                                                                    </div>

                                                                                                                    <div class="course-language-caption">
                                                                                                                        <h5 class="course-language-name">
                                                                                                                            Japanese Courses </h5>
                                                                                                                        <div class="course-language-info">
                                                                                                                            <div class="course-language-flag">
                                                                                                                                <img src="https://live-edumall.thememove.com/wp-content/uploads/2020/06/japan.svg" alt="japan" />
                                                                                                                            </div>

                                                                                                                            <div class="course-language-count">
                                                                                                                                1 program </div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </a>
                                                                                                            </div>
                                                                                                            <div class="grid-item">
                                                                                                                <a href="https://live-edumall.thememove.com/course-language/korean/" class="edumall-box course-language-wrapper cat-link link-secret">

                                                                                                                    <div class="course-thumb-wrap">
                                                                                                                        <div class="edumall-image image">
                                                                                                                            <img src="https://live-edumall.thememove.com/wp-content/uploads/2020/06/course-language-korean-thumbnail-480x364.jpg" alt="course-language-korean-thumbnail" width="480" />
                                                                                                                        </div>
                                                                                                                    </div>

                                                                                                                    <div class="course-language-caption">
                                                                                                                        <h5 class="course-language-name">
                                                                                                                            Korean Courses </h5>
                                                                                                                        <div class="course-language-info">
                                                                                                                            <div class="course-language-flag">
                                                                                                                                <img src="https://live-edumall.thememove.com/wp-content/uploads/2020/06/south-korea.svg" alt="south-korea" />
                                                                                                                            </div>

                                                                                                                            <div class="course-language-count">
                                                                                                                                5 programs </div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </a>
                                                                                                            </div>
                                                                                                            <div class="grid-item">
                                                                                                                <a href="https://live-edumall.thememove.com/course-language/russian/" class="edumall-box course-language-wrapper cat-link link-secret">

                                                                                                                    <div class="course-thumb-wrap">
                                                                                                                        <div class="edumall-image image">
                                                                                                                            <img src="https://live-edumall.thememove.com/wp-content/uploads/2020/06/course-language-russian-thumbnail-480x364.jpg" alt="course-language-russian-thumbnail" width="480" />
                                                                                                                        </div>
                                                                                                                    </div>

                                                                                                                    <div class="course-language-caption">
                                                                                                                        <h5 class="course-language-name">
                                                                                                                            Russian Courses </h5>
                                                                                                                        <div class="course-language-info">
                                                                                                                            <div class="course-language-flag">
                                                                                                                                <img src="https://live-edumall.thememove.com/wp-content/uploads/2020/06/russia.svg" alt="russia" />
                                                                                                                            </div>

                                                                                                                            <div class="course-language-count">
                                                                                                                                6 programs </div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </a>
                                                                                                            </div>
                                                                                                            <div class="grid-item">
                                                                                                                <a href="https://live-edumall.thememove.com/course-language/spanish/" class="edumall-box course-language-wrapper cat-link link-secret">

                                                                                                                    <div class="course-thumb-wrap">
                                                                                                                        <div class="edumall-image image">
                                                                                                                            <img src="https://live-edumall.thememove.com/wp-content/uploads/2020/06/course-language-spanish-thumbnail-480x364.jpg" alt="course-language-spanish-thumbnail" width="480" />
                                                                                                                        </div>
                                                                                                                    </div>

                                                                                                                    <div class="course-language-caption">
                                                                                                                        <h5 class="course-language-name">
                                                                                                                            Spanish Courses </h5>
                                                                                                                        <div class="course-language-info">
                                                                                                                            <div class="course-language-flag">
                                                                                                                                <img src="https://live-edumall.thememove.com/wp-content/uploads/2020/06/spain.svg" alt="spain" />
                                                                                                                            </div>

                                                                                                                            <div class="course-language-count">
                                                                                                                                4 programs </div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </a>
                                                                                                            </div>

                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="elementor-element elementor-element-777e6a1 elementor-align-center elementor-invisible elementor-widget elementor-widget-tm-button" data-id="777e6a1" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;edumallFadeInUp&quot;}" data-widget_type="tm-button.default">
                                                                                                <div class="elementor-widget-container">
                                                                                                    <div class="tm-button-wrapper">
                                                                                                        <a href="https://live-edumall.thememove.com/courses/" class="tm-button-link tm-button style-flat tm-button-nm" role="button">
                                                                                                            <div class="button-content-wrapper">

                                                                                                                <span class="button-text">View All Courses</span>

                                                                                                            </div>

                                                                                                        </a>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </section>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>




                                    
                                  
                                    </div>
                                </div>
                            </div>
                        </article>


                    </div>


                </div>
            </div>
        </div>
    </div><!-- /.content-wrapper -->




    <!-- footer -->

    @section('footer')
    @include('frontOffice.inc.footer')
    @endsection

    <!-- /footer -->
</div><!-- /.site -->

@endsection