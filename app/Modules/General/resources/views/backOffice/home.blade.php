@extends('backOffice.layout')

@section('head')
    @include('backOffice.inc.head',
    ['title' => ' - Administration',
    'description' => 'Espace Administratif - '
    ])
@endsection

@section('header')
    @include('backOffice.inc.header')
@endsection

@section('sidebar')
    @include('backOffice.inc.sidebar', [
        'current' => 'dashboard'
    ])
@endsection

@section('content')

    <div class="breadcrumb">
        <h1>مرحبا !</h1>
        <ul>
            <li><a href="#">لوحة التحكم</a></li>
        </ul>
    </div>

    <div class="separator-breadcrumb border-top"></div>

    <div class="row">         <!-- ICON BG-->
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
                    <div class="card-body text-center"><i class="i-Add-User"></i>
                        <div class="content">
                            <p class="text-muted mt-2 mb-0">المستعملين</p>
                            <p class="text-primary text-24 line-height-1 mb-2"> 300 </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
                    <div class="card-body text-center"><i class="i-Book"></i>
                        <div class="content">
                            <p class="text-muted mt-2 mb-0"> الدورات </p>
                            <p class="text-primary text-24 line-height-1 mb-2"> 347</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
                    <div class="card-body text-center"><i class="i-Love-User"></i>
                        <div class="content">
                            <p class="text-muted mt-2 mb-0">المسارات</p>
                            <p class="text-primary text-24 line-height-1 mb-2"> 10 </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
                    <div class="card-body text-center"><i class="i-Find-User"></i>
                        <div class="content">
                            <p class="text-muted mt-2 mb-0">المشاريع</p>
                            <p class="text-primary text-24 line-height-1 mb-2"> 10 </p>
                        </div>
                    </div>
                </div>
            </div>
       
    </div>



@endsection