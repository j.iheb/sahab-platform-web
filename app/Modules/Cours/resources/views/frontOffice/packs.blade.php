@extends('frontOffice.layout')

@section('head')
@include('frontOffice.inc.head')
@endsection

@section('header')
@include('frontOffice.inc.header')
@endsection



@section('content')

<!-- Page Content -->
<div id="body-container">

    <div class="intro bg-primary text-white">
        <div class="container">
            <div class="row pt-5 text-center text-md-right align-items-center">
                <div class="col-md-6 order-1 order-md-0 ">
                    <img class="img-fluid" src="https://www.flexcourses.com/imgs/packages.png" alt="">
                </div>
                <div class="col-md-6 order-0 order-md-1 text-center text-md-right">
                    <h3 class="pb-3">طريقك المرتب للتعلم</h3>
                    <h6 class="h5 font-head mb-0">هنا رتبنا لك دورات تتوافق مع بعض لتكوين مهارات متكاملة</h6>

                    <h6 class="h5 font-head mb-0">ومن هنا تخرج بفائدة اكبر وتوفر اكثر</h6>
                    <h6 class="h4 font-head mt-5">مو قلنا لك "تعلم ممتع"؟</h6>
                </div>
            </div>
        </div>
        <svg class="intro-svg" xmlns="http://www.w3.org/2000/svg" viewBox="0 127 1440 193">
            <path fill="#fff" fill-opacity="1" d="M0,128L60,154.7C120,181,240,235,360,224C480,213,600,139,720,144C840,
                  149,960,235,1080,250.7C1200,267,1320,213,1380,186.7L1440,160L1440,320L1380,320C1320,320,1200,320,
                  1080,320C960,320,840,320,720,320C600,320,480,320,360,320C240,320,120,320,60,320L0,320Z">

            </path>
        </svg>
    </div>
    <div class="container container-lg-fluid">
        <div class="row pb-4">
            <h4 class="col-12 bg-primary-light p-5 text-center">
                لا يوجد باقات متاحة الآن
            </h4>
        </div>
        <div id="registerModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
            <div class="modal-dialog modal-md mt-5" role="document">

                <div class="modal-content">
                    <button type="button" class="close login-register-modal-close rounded-circle bg-white shadow-sm" data-dismiss="modal" aria-label="Close">
                        <i class="fa fa-times" aria-hidden="true"></i>
                    </button>
                    <ul class="nav nav-tabs nav-fill" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="login-tab" data-toggle="tab" title="دخول" role="tab" href="#login">دخول</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="register-tab" data-toggle="tab" title="تسجيل" role="tab" href="#register">تسجيل</a>
                        </li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane show active" id="login" role="tabpanel">
                            <div class="card p-3 p-md-5 rounded-0 border-top-0">
                                <div class="panel-body pt-3">


                                    <form class="form-horizontal" role="form" method="POST" action="#">
                                        <input type="hidden" name="_token" value="ggo6KsqFOnjdkAUdKJwiUsQNi0d6tOr8SvWXdtNh">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <input type="email" class="form-control py-md-4 rounded-0 text-right" name="email" placeholder="البريد الإلكتروني" value="" required>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <input type="password" class="form-control py-md-4 rounded-0 text-right" placeholder="كلمة المرور" name="password" required>
                                            </div>
                                        </div>

                                        <div class="form-group mb-0">
                                            <div class="col-md-12 text-center">
                                                <label>
                                                    <input type="checkbox" name="remember"> تذكرني
                                                </label>
                                            </div>
                                        </div>

                                        <div class="small p-3">
                                            بدخولك لحسابك فإنك توافق على <a href="page/privacy-policy">سياسة الخصوصية</a> و<a href="page/terms">شروط الاستخدام</a>.
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <button type="submit" class="btn btn-block btn-primary">دخول</button>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12 pb-md-4 text-center">
                                                <a href="#" title="هل نسيت كلمة المرور؟">هل نسيت كلمة المرور؟</a>
                                            </div>
                                        </div>
                                    </form>

                                    <hr>

                                    <p class="text-secondary">
                                        <small>أو بإمكانك الدخول باستخدام:</small>
                                    </p>
                                    <style>
                                        .btn-twitter {
                                            border: 1px solid #1DA1F2;
                                            color: #1DA1F2;
                                            display: block;
                                            background: url('/imgs/t2-logo.png') white;
                                        }

                                        .btn-google {
                                            border: 1px solid #EA4335;
                                            color: #EA4335;
                                            display: block;
                                            background: url('/imgs/g-logo.svg') white;
                                        }

                                        .btn-twitter:hover {
                                            color: #1DA1F2;
                                        }

                                        .btn-google:hover {
                                            color: #EA4335;
                                        }

                                        .social-btn {
                                            background-position: right 7px center;
                                            background-repeat: no-repeat;
                                            background-size: 28px;
                                            padding-right: 49px;
                                            text-align: right;
                                            margin-bottom: 11px;
                                        }

                                        .social-btn:hover {
                                            opacity: 0.8;
                                        }
                                    </style>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <a href="#" title="تويتر" class="social-btn btn btn-twitter">
                                                تويتر
                                            </a>
                                        </div>
                                        <div class="col-md-6">
                                            <a href="#" title="جوجل" class="social-btn btn btn-google">
                                                جوجل
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="register" role="tabpanel">
                            <div class="card p-3 p-md-5 rounded-0 border-top-0">
                                <div class="panel-body pt-3">

                                    <form class="form-horizontal" role="form" method="POST" action="#">
                                        <input type="hidden" name="_token" value="ggo6KsqFOnjdkAUdKJwiUsQNi0d6tOr8SvWXdtNh">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <input id="name" placeholder="الاسم" type="text" class="form-control py-md-4 rounded-0 text-right" name="name" value="" required>
                                                <span class="small text-secondary">الاسم كاملا (يظهر للإدارة وفي شهاداتك فقط)</span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <input id="name" placeholder="اسم المستخدم" type="text" class="form-control py-md-4 rounded-0 text-right" name="nickname" value="" required>
                                                <span class="small text-secondary">اسم العرض الخاص بك (أحرف وأرقام إنجليزية فقط)</span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <input id="email" placeholder="البريد الإلكتروني" type="email" class="form-control py-md-4 rounded-0 text-right" name="email" value="" required>

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <input placeholder="كلمة المرور" id="password" type="password" class="form-control py-md-4 rounded-0 text-right" name="password" required>

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <input id="password-confirm" placeholder="تأكيد كلمة المرور" type="password" class="form-control py-md-4 rounded-0 text-right" name="password_confirmation" required>
                                            </div>
                                        </div>

                                        <div class="small p-3">
                                            بتسجيلك فإنك توافق على <a href="page/privacy-policy">سياسة الخصوصية</a> و<a href="page/terms">شروط الاستخدام</a>.
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <button type="submit" class="btn btn-block btn-primary">
                                                    تسجيل
                                                </button>
                                            </div>
                                            <div class="col-md-12 pt-3 pb-md-4 text-center">
                                                <a class="register-link" title="مسجل مسبقا؟ سجل دخولك من هنا." href="#login">مسجل مسبقا؟ سجل دخولك من هنا.</a>
                                            </div>
                                        </div>
                                    </form>

                                    <hr>

                                    <p class="text-secondary">
                                        <small>أو بإمكانك الدخول باستخدام:</small>
                                    </p>
                                    <style>
                                        .btn-twitter {
                                            border: 1px solid #1DA1F2;
                                            color: #1DA1F2;
                                            display: block;
                                            background: url('/imgs/t2-logo.png') white;
                                        }

                                        .btn-google {
                                            border: 1px solid #EA4335;
                                            color: #EA4335;
                                            display: block;
                                            background: url('/imgs/g-logo.svg') white;
                                        }

                                        .btn-twitter:hover {
                                            color: #1DA1F2;
                                        }

                                        .btn-google:hover {
                                            color: #EA4335;
                                        }

                                        .social-btn {
                                            background-position: right 7px center;
                                            background-repeat: no-repeat;
                                            background-size: 28px;
                                            padding-right: 49px;
                                            text-align: right;
                                            margin-bottom: 11px;
                                        }

                                        .social-btn:hover {
                                            opacity: 0.8;
                                        }
                                    </style>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <a href="#" title="تويتر" class="social-btn btn btn-twitter">
                                                تويتر
                                            </a>
                                        </div>
                                        <div class="col-md-6">
                                            <a href="#" title="جوجل" class="social-btn btn btn-google">
                                                جوجل
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


</div>
<!-- /.container -->

@endsection