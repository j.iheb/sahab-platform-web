@extends('frontOffice.layout')

@section('head')
@include('frontOffice.inc.head',
['title' => ' - Administration',
'description' => 'Espace Administratif - '
])
@endsection




@section('content')


<div id="page" class="site">
    <div class="content-wrapper">

        @section('header')
        @include('frontOffice.inc.header')
        @endsection


        <div id="page-content" class="page-content">
            <div class="container">
                <div class="row">


                    <div id="page-main-content" class="page-main-content">

                        <div class="rich-snippet display-none">
                            <h1 class="entry-title">Course Hub</h1> <span class="published">August 27, 2020</span>
                            <span class="updated" data-time="2020-11-12 2:29">2020-11-12 2:29</span>
                        </div>

                        <article id="post-619" class="post-619 page type-page status-publish hentry pmpro-has-access post-no-thumbnail">
                            <h2 class="screen-reader-text">Course Hub</h2>
                            <div data-elementor-type="wp-page" data-elementor-id="619" class="elementor elementor-619" data-elementor-settings="[]">
                                <div class="elementor-inner">
                                    <div class="elementor-section-wrap">
                                      <br> <br> <br>
                                        <section class="elementor-section elementor-top-section elementor-element elementor-element-268e99d elementor-section-boxed elementor-section-gap-beside-yes elementor-section-height-default elementor-section-height-default elementor-section-column-vertical-align-stretch" data-id="268e99d" data-element_type="section" id="section-top-banners" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}" >
                                            <div class="elementor-container elementor-column-gap-extended">
                                                <div class="elementor-row">
                                                    <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-70f21c0 elementor-invisible" data-id="70f21c0" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;edumallFadeInUp&quot;}">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <section class="elementor-section elementor-inner-section elementor-element elementor-element-a8eaee2 elementor-section-content-bottom elementor-section-boxed elementor-section-gap-beside-yes elementor-section-height-default elementor-section-height-default elementor-section-column-vertical-align-stretch" data-id="a8eaee2" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                                                                    <div class="elementor-container elementor-column-gap-extended">
                                                                        <div class="elementor-row">
                                                                            <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-158f828" data-id="158f828" data-element_type="column">
                                                                                <div class="elementor-column-wrap elementor-element-populated">
                                                                                    <div class="elementor-widget-wrap">
                                                                                        <div class="elementor-element elementor-element-3739a09 elementor-widget elementor-widget-spacer" data-id="3739a09" data-element_type="widget" data-widget_type="spacer.default">
                                                                                            <div class="elementor-widget-container">
                                                                                                <div class="elementor-spacer">
                                                                                                    <div class="elementor-spacer-inner"></div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="elementor-element elementor-element-aa3a592 elementor-widget elementor-widget-tm-heading" data-id="aa3a592" data-element_type="widget" data-widget_type="tm-heading.default">
                                                                                            <div class="elementor-widget-container">
                                                                                                <div class="tm-modern-heading">
                                                                                                    <div class="heading-secondary-wrap">
                                                                                                        <h3 class="heading-secondary elementor-heading-title">شهادات جديدة</h3>
                                                                                                    </div>

                                                                                                    <div class="heading-primary-wrap">
                                                                                                        <h3 class="heading-primary elementor-heading-title">دورات عبر الإنترنت من منصة السحاب   </h3>
                                                                                                    </div>


                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <br> <br>
                                                                                        <div class="elementor-element elementor-element-258d1c8 elementor-invisible elementor-widget elementor-widget-tm-button" data-id="258d1c8" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;none&quot;}" data-widget_type="tm-button.default">
                                                                                            <div class="elementor-widget-container">
                                                                                                <div class="tm-button-wrapper">
                                                                                                <br> <br>
                                                                                                    <a href="#" class="tm-button-link tm-button style-flat tm-button-xs" role="button">
                                                                                                        <div class="button-content-wrapper">

                                                                                                            <span class="button-text">اكتشف المزيد</span>

                                                                                                        </div>

                                                                                                    </a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="elementor-element elementor-element-9d77e25 elementor-widget elementor-widget-spacer" data-id="9d77e25" data-element_type="widget" data-widget_type="spacer.default">
                                                                                            <div class="elementor-widget-container">
                                                                                                <div class="elementor-spacer">
                                                                                                    <div class="elementor-spacer-inner"></div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-feb1a64" data-id="feb1a64" data-element_type="column">
                                                                                <div class="elementor-column-wrap elementor-element-populated">
                                                                                    <div class="elementor-widget-wrap">
                                                                                        <div class="elementor-element elementor-element-ee988ec elementor-widget elementor-widget-image" data-id="ee988ec" data-element_type="widget" data-widget_type="image.default">
                                                                                            <div class="elementor-widget-container">
                                                                                                <div class="elementor-image">
                                                                                                    <img width="407" height="230" src="https://live-edumall.thememove.com/wp-content/uploads/2020/08/banner-image-04.png" class="attachment-full size-full" alt="" loading="lazy" srcset="https://live-edumall.thememove.com/wp-content/uploads/2020/08/banner-image-04.png 407w, https://live-edumall.thememove.com/wp-content/uploads/2020/08/banner-image-04-300x170.png 300w, https://live-edumall.thememove.com/wp-content/uploads/2020/08/banner-image-04-400x226.png 400w" sizes="(max-width: 407px) 100vw, 407px" />
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </section>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-312d7df elementor-invisible" data-id="312d7df" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;edumallFadeInUp&quot;}">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <section class="elementor-section elementor-inner-section elementor-element elementor-element-d5cbaf1 elementor-section-content-middle elementor-section-boxed elementor-section-gap-beside-yes elementor-section-height-default elementor-section-height-default elementor-section-column-vertical-align-stretch" data-id="d5cbaf1" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                                                                    <div class="elementor-container elementor-column-gap-extended">
                                                                        <div class="elementor-row">
                                                                            <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-9b4cd8c" data-id="9b4cd8c" data-element_type="column">
                                                                                <div class="elementor-column-wrap elementor-element-populated">
                                                                                    <div class="elementor-widget-wrap">
                                                                                        <div class="elementor-element elementor-element-a61af7d elementor-widget elementor-widget-spacer" data-id="a61af7d" data-element_type="widget" data-widget_type="spacer.default">
                                                                                            <div class="elementor-widget-container">
                                                                                                <div class="elementor-spacer">
                                                                                                    <div class="elementor-spacer-inner"></div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="elementor-element elementor-element-6aea1ca elementor-widget elementor-widget-tm-heading" data-id="6aea1ca" data-element_type="widget" data-widget_type="tm-heading.default">
                                                                                            <div class="elementor-widget-container">
                                                                                                <div class="tm-modern-heading">
                                                                                                    <div class="heading-secondary-wrap">
                                                                                                        <h3 class="heading-secondary elementor-heading-title">مجموعة جديدة</h3>
                                                                                                    </div>

                                                                                                    <div class="heading-primary-wrap">
                                                                                                        <h3 class="heading-primary elementor-heading-title">إتقان مهارات البرمجة الخاصة بك  </h3>
                                                                                                    </div>


                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <br> <br>
                                                                                        <div class="elementor-element elementor-element-30b9c3f elementor-invisible elementor-widget elementor-widget-tm-button" data-id="30b9c3f" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;none&quot;}" data-widget_type="tm-button.default">
                                                                                            <div class="elementor-widget-container">
                                                                                                <div class="tm-button-wrapper">
                                                                                                <br> <br>
                                                                                                    <a href="https://live-edumall.thememove.com/courses/" class="tm-button-link tm-button style-flat tm-button-xs" role="button">
                                                                                                        <div class="button-content-wrapper">

                                                                                                            <span class="button-text">عرض الدورات</span>

                                                                                                        </div>

                                                                                                    </a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="elementor-element elementor-element-f461755 elementor-widget elementor-widget-spacer" data-id="f461755" data-element_type="widget" data-widget_type="spacer.default">
                                                                                            <div class="elementor-widget-container">
                                                                                                <div class="elementor-spacer">
                                                                                                    <div class="elementor-spacer-inner"></div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-be504ef" data-id="be504ef" data-element_type="column">
                                                                                <div class="elementor-column-wrap elementor-element-populated">
                                                                                    <div class="elementor-widget-wrap">
                                                                                        <div class="elementor-element elementor-element-86e5dc2 edumall-animation-zoom-in elementor-align-center elementor-widget elementor-widget-tm-popup-video" data-id="86e5dc2" data-element_type="widget" data-widget_type="tm-popup-video.default">
                                                                                            <div class="elementor-widget-container">
                                                                                                <div class="tm-popup-video type-poster tm-popup-video-image-play">
                                                                                                    <a class="video-link edumall-box link-secret" href="https://www.youtube.com/watch?v=XHOmBV4js_E">

                                                                                                        <div class="video-poster">
                                                                                                            <div class="edumall-image">
                                                                                                                <img src="https://live-edumall.thememove.com/wp-content/uploads/2020/07/course-thumbnail-09-240x150.jpg" alt="course-thumbnail-09" width="240" />
                                                                                                            </div>

                                                                                                            <div class="video-overlay"></div>

                                                                                                            <div class="video-button">
                                                                                                                <div class="video-button-play">

                                                                                                                    <div class="video-play video-play-image">
                                                                                                                        <img src="https://live-edumall.thememove.com/wp-content/uploads/2020/08/youtube-play-button.png" alt="youtube-play-button" />
                                                                                                                    </div>
                                                                                                                </div>

                                                                                                            </div>
                                                                                                        </div>


                                                                                                    </a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </section>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                        
                                       

                                        <br> <br> <br>
                                        <section class="elementor-section elementor-top-section elementor-element elementor-element-6eaeefc elementor-section-stretched elementor-section-boxed elementor-section-gap-beside-yes elementor-section-height-default elementor-section-height-default elementor-section-column-vertical-align-stretch" data-id="6eaeefc" data-element_type="section" data-settings="{&quot;stretch_section&quot;:&quot;section-stretched&quot;}">
                                            <div class="elementor-container elementor-column-gap-extended">
                                                <div class="elementor-row">
                                                    <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-d987668" data-id="d987668" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <section class="elementor-section elementor-inner-section elementor-element elementor-element-c5f5614 elementor-section-gap-beside-no elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section-column-vertical-align-stretch" data-id="c5f5614" data-element_type="section">
                                                                    <div class="elementor-container elementor-column-gap-extended">
                                                                        <div class="elementor-row">
                                                                            <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-e94a181" data-id="e94a181" data-element_type="column">
                                                                                <div class="elementor-column-wrap elementor-element-populated">
                                                                                    <div class="elementor-widget-wrap">
                                                                                        <div class="elementor-element elementor-element-01dfec3 edumall-modern-heading-style-01 elementor-invisible elementor-widget elementor-widget-tm-heading" data-id="01dfec3" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;edumallFadeInUp&quot;}" data-widget_type="tm-heading.default">
                                                                                            <div class="elementor-widget-container">
                                                                                                <div class="tm-modern-heading">

                                                                                                    <div class="heading-primary-wrap">
                                                                                                        <h3 class="heading-primary elementor-heading-title">تصفح <mark> الموضوعات </mark></h3>
                                                                                                    </div>


                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                         
                                                                        </div>
                                                                    </div>
                                                                </section>
                                                                <br> <br> <br>
                                                                <div class="elementor-element elementor-element-3c05b92 edumall-animation-zoom-in bullets-v-align-below elementor-invisible elementor-widget elementor-widget-tm-course-category-carousel" data-id="3c05b92" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;edumallFadeInUp&quot;}" data-widget_type="tm-course-category-carousel.default">
                                                                    <div class="elementor-widget-container">

                                                                        <div class="tm-swiper tm-slider-widget" data-lg-items="5" data-md-items="3" data-sm-items="auto-fixed" data-lg-gutter="30" data-md-gutter="" data-sm-gutter="" data-loop="1" data-simulate-touch="1" data-speed="1000" data-effect="slide">
                                                                            <div class="swiper-inner">


                                                                                <div class="swiper-container">
                                                                                    <div class="swiper-wrapper">
                                                                                        <div class="swiper-slide">
                                                                                            <a class="edumall-box link-secret" href="https://live-edumall.thememove.com/course-category/art-design/">
                                                                                                <div class="edumall-image">
                                                                                                    <img src="https://live-edumall.thememove.com/wp-content/uploads/2020/09/art-n-design-260x320.jpg" alt="art-n-design" width="260" />
                                                                                                    <div class="category-info">
                                                                                                        <h6 class="category-name"> الفن و التصميم</h6>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </a>
                                                                                        </div>
                                                                                        <div class="swiper-slide">
                                                                                            <a class="edumall-box link-secret" href="https://live-edumall.thememove.com/course-category/business/">
                                                                                                <div class="edumall-image">
                                                                                                    <img src="https://live-edumall.thememove.com/wp-content/uploads/2020/09/business-260x320.jpg" alt="business" width="260" />
                                                                                                    <div class="category-info">
                                                                                                        <h6 class="category-name">الأعمال</h6>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </a>
                                                                                        </div>
                                                                                        <div class="swiper-slide">
                                                                                            <a class="edumall-box link-secret" href="https://live-edumall.thememove.com/course-category/data-science/">
                                                                                                <div class="edumall-image">
                                                                                                    <img src="https://live-edumall.thememove.com/wp-content/uploads/2020/09/data-science-260x320.jpg" alt="data-science" width="260" />
                                                                                                    <div class="category-info">
                                                                                                        <h6 class="category-name">علم الألة</h6>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </a>
                                                                                        </div>
                                                                                        <div class="swiper-slide">
                                                                                            <a class="edumall-box link-secret" href="https://live-edumall.thememove.com/course-category/development/">
                                                                                                <div class="edumall-image">
                                                                                                    <img src="https://live-edumall.thememove.com/wp-content/uploads/2020/09/development-260x320.jpg" alt="development" width="260" />
                                                                                                    <div class="category-info">
                                                                                                        <h6 class="category-name">برمجة</h6>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </a>
                                                                                        </div>
                                                                                        <div class="swiper-slide">
                                                                                            <a class="edumall-box link-secret" href="https://live-edumall.thememove.com/course-category/finance/">
                                                                                                <div class="edumall-image">
                                                                                                    <img src="https://live-edumall.thememove.com/wp-content/uploads/2020/09/finance-260x320.jpg" alt="finance" width="260" />
                                                                                                    <div class="category-info">
                                                                                                        <h6 class="category-name">صيانة</h6>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </a>
                                                                                        </div>
                                                                                        <div class="swiper-slide">
                                                                                            <a class="edumall-box link-secret" href="https://live-edumall.thememove.com/course-category/health-fitness/">
                                                                                                <div class="edumall-image">
                                                                                                    <img src="https://live-edumall.thememove.com/wp-content/uploads/2020/09/health-n-fitness-260x320.jpg" alt="health-n-fitness" width="260" />
                                                                                                    <div class="category-info">
                                                                                                        <h6 class="category-name">صحة و رياضة</h6>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </a>
                                                                                        </div>
                                                                                        <div class="swiper-slide">
                                                                                            <a class="edumall-box link-secret" href="https://live-edumall.thememove.com/course-category/lifestyle/">
                                                                                                <div class="edumall-image">
                                                                                                    <img src="https://live-edumall.thememove.com/wp-content/uploads/2020/09/lifestyle-260x320.jpg" alt="lifestyle" width="260" />
                                                                                                    <div class="category-info">
                                                                                                        <h6 class="category-name"> نمط حياة</h6>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </a>
                                                                                        </div>
                                                                                        <div class="swiper-slide">
                                                                                            <a class="edumall-box link-secret" href="https://live-edumall.thememove.com/course-category/marketing/">
                                                                                                <div class="edumall-image">
                                                                                                    <img src="https://live-edumall.thememove.com/wp-content/uploads/2020/09/marketing-260x320.jpg" alt="marketing" width="260" />
                                                                                                    <div class="category-info">
                                                                                                        <h6 class="category-name">تسويق</h6>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </a>
                                                                                        </div>
                                                                                        <div class="swiper-slide">
                                                                                            <a class="edumall-box link-secret" href="https://live-edumall.thememove.com/course-category/music/">
                                                                                                <div class="edumall-image">
                                                                                                    <img src="https://live-edumall.thememove.com/wp-content/uploads/2020/09/music-260x320.jpg" alt="music" width="260" />
                                                                                                    <div class="category-info">
                                                                                                        <h6 class="category-name">موسيقى</h6>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </a>
                                                                                        </div>
                                                                                        <div class="swiper-slide">
                                                                                            <a class="edumall-box link-secret" href="https://live-edumall.thememove.com/course-category/personal-development/">
                                                                                                <div class="edumall-image">
                                                                                                    <img src="https://live-edumall.thememove.com/wp-content/uploads/2020/09/personal-development-260x320.jpg" alt="personal-development" width="260" />
                                                                                                    <div class="category-info">
                                                                                                        <h6 class="category-name"> تطوير تفسي</h6>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </a>
                                                                                        </div>
                                                                                        <div class="swiper-slide">
                                                                                            <a class="edumall-box link-secret" href="https://live-edumall.thememove.com/course-category/photography/">
                                                                                                <div class="edumall-image">
                                                                                                    <img src="https://live-edumall.thememove.com/wp-content/uploads/2020/09/photography-260x320.jpg" alt="photography" width="260" />
                                                                                                    <div class="category-info">
                                                                                                        <h6 class="category-name">تصوير</h6>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </a>
                                                                                        </div>
                                                                                        <div class="swiper-slide">
                                                                                            <a class="edumall-box link-secret" href="https://live-edumall.thememove.com/course-category/teaching-academics/">
                                                                                                <div class="edumall-image">
                                                                                                    <img src="https://live-edumall.thememove.com/wp-content/uploads/2020/09/teaching-n-academics-260x320.jpg" alt="teaching-n-academics" width="260" />
                                                                                                    <div class="category-info">
                                                                                                        <h6 class="category-name"> تدريس و أكاديميات</h6>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </a>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>


                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>

                                            <br> <br> <br>
                                        <section class="elementor-section elementor-top-section elementor-element elementor-element-78d36de elementor-section-stretched elementor-section-boxed elementor-section-gap-beside-yes elementor-section-height-default elementor-section-height-default elementor-section-column-vertical-align-stretch" data-id="78d36de" data-element_type="section" data-settings="{&quot;stretch_section&quot;:&quot;section-stretched&quot;}">
                                            <div class="elementor-container elementor-column-gap-extended">
                                                <div class="elementor-row">
                                                    <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-0a8d1fb" data-id="0a8d1fb" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <section class="elementor-section elementor-inner-section elementor-element elementor-element-1aae62d elementor-section-gap-beside-no elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section-column-vertical-align-stretch" data-id="1aae62d" data-element_type="section">
                                                                    <div class="elementor-container elementor-column-gap-extended">
                                                                        <div class="elementor-row">
                                                                            <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-81a4112" data-id="81a4112" data-element_type="column">
                                                                                <div class="elementor-column-wrap elementor-element-populated">
                                                                                    <div class="elementor-widget-wrap">
                                                                                        <div class="elementor-element elementor-element-ac2de46 edumall-modern-heading-style-01 elementor-invisible elementor-widget elementor-widget-tm-heading" data-id="ac2de46" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;edumallFadeInUp&quot;}" data-widget_type="tm-heading.default">
                                                                                            <div class="elementor-widget-container">
                                                                                                <div class="tm-modern-heading">

                                                                                                    <div class="heading-primary-wrap">
                                                                                                        <h3 class="heading-primary elementor-heading-title">سحاب  <mark>إرشاد</mark></h3>
                                                                                                    </div>


                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                       
                              
                                                                        </div>
                                                                    </div>
                                                                </section>
                                                                <section class="elementor-section elementor-inner-section elementor-element elementor-element-b1a0699 elementor-section-boxed elementor-section-gap-beside-yes elementor-section-height-default elementor-section-height-default elementor-section-column-vertical-align-stretch" data-id="b1a0699" data-element_type="section">
                                                                    <div class="elementor-container elementor-column-gap-extended">
                                                                        <div class="elementor-row">
                                                                            <div class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-04b368e" data-id="04b368e" data-element_type="column">
                                                                                <div class="elementor-column-wrap elementor-element-populated">
                                                                                    <div class="elementor-widget-wrap">
                                                                                        <div class="elementor-element elementor-element-79f538e elementor-invisible elementor-widget elementor-widget-tm-image-box" data-id="79f538e" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;edumallFadeInUp&quot;}" data-widget_type="tm-image-box.default">
                                                                                            <div class="elementor-widget-container">
                                                                                                <div class="tm-image-box edumall-box style- image-position-top content-alignment-">
                                                                                                    <div class="content-wrap">

                                                                                                        <div class="image-wrap">
                                                                                                            <div class="edumall-image image">
                                                                                                                <img src="https://live-edumall.thememove.com/wp-content/uploads/2020/08/image-box-01.png" alt="image-box-01" />
                                                                                                            </div>

                                                                                                        </div>

                                                                                                        <div class="box-caption-wrap">
                                                                                                            <div class="box-caption">
                                                                                                                <h3 class="title">تعلم المهارات الأساسية</h3>
                                                                                                                <div class="description">
                                                                                                                مثل التصميم الجرافيكي وتحليلات الأعمال والترميز وغير ذلك الكثير</div>

                                                                                                            </div>
                                                                                                        </div>

                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-e94fbcd" data-id="e94fbcd" data-element_type="column">
                                                                                <div class="elementor-column-wrap elementor-element-populated">
                                                                                    <div class="elementor-widget-wrap">
                                                                                        <div class="elementor-element elementor-element-cbdca97 elementor-invisible elementor-widget elementor-widget-tm-image-box" data-id="cbdca97" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;edumallFadeInUp&quot;}" data-widget_type="tm-image-box.default">
                                                                                            <div class="elementor-widget-container">
                                                                                                <div class="tm-image-box edumall-box style- image-position-top content-alignment-">
                                                                                                    <div class="content-wrap">

                                                                                                        <div class="image-wrap">
                                                                                                            <div class="edumall-image image">
                                                                                                                <img src="https://live-edumall.thememove.com/wp-content/uploads/2020/08/image-box-02.png" alt="image-box-02" />
                                                                                                            </div>

                                                                                                        </div>

                                                                                                        <div class="box-caption-wrap">
                                                                                                            <div class="box-caption">
                                                                                                                <h3 class="title">احصل على الشهادات والدرجات</h3>
                                                                                                                <div class="description">
                                                                                                                من أفضل المؤسسات والجامعات ذات السمعة العالية على مستوى العالم</div>

                                                                                                            </div>
                                                                                                        </div>

                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-f23b60c" data-id="f23b60c" data-element_type="column">
                                                                                <div class="elementor-column-wrap elementor-element-populated">
                                                                                    <div class="elementor-widget-wrap">
                                                                                        <div class="elementor-element elementor-element-aebdb7c elementor-invisible elementor-widget elementor-widget-tm-image-box" data-id="aebdb7c" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;edumallFadeInUp&quot;}" data-widget_type="tm-image-box.default">
                                                                                            <div class="elementor-widget-container">
                                                                                                <div class="tm-image-box edumall-box style- image-position-top content-alignment-">
                                                                                                    <div class="content-wrap">

                                                                                                        <div class="image-wrap">
                                                                                                            <div class="edumall-image image">
                                                                                                                <img src="https://live-edumall.thememove.com/wp-content/uploads/2020/08/image-box-03.png" alt="image-box-03" />
                                                                                                            </div>

                                                                                                        </div>

                                                                                                        <div class="box-caption-wrap">
                                                                                                            <div class="box-caption">
                                                                                                                <h3 class="title">استعد للوظيفة التالية</h3>
                                                                                                                <div class="description">
                                                                                                                    مع المتطلبات العالية لإتقان مهارات جديدة في تكنولوجيا المعلومات والتحليلات والمزيد
                                                                                                                </div>

                                                                                                            </div>
                                                                                                        </div>

                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-54d1a86" data-id="54d1a86" data-element_type="column">
                                                                                <div class="elementor-column-wrap elementor-element-populated">
                                                                                    <div class="elementor-widget-wrap">
                                                                                        <div class="elementor-element elementor-element-fc6ea47 elementor-invisible elementor-widget elementor-widget-tm-image-box" data-id="fc6ea47" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;edumallFadeInUp&quot;}" data-widget_type="tm-image-box.default">
                                                                                            <div class="elementor-widget-container">
                                                                                                <div class="tm-image-box edumall-box style- image-position-top content-alignment-">
                                                                                                    <div class="content-wrap">

                                                                                                        <div class="image-wrap">
                                                                                                            <div class="edumall-image image">
                                                                                                                <img src="https://live-edumall.thememove.com/wp-content/uploads/2020/08/image-box-04.png" alt="image-box-04" />
                                                                                                            </div>

                                                                                                        </div>

                                                                                                        <div class="box-caption-wrap">
                                                                                                            <div class="box-caption">
                                                                                                                <h3 class="title">ماجستير في مجالات مختلفة</h3>
                                                                                                                <div class="description">
                                                                                                                مع منصة السحاب الآلاف من الدورات بتوجيه من كبار الخبراء </div>

                                                                                                            </div>
                                                                                                        </div>

                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </section>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                  
                                    </div>
                                </div>
                            </div>
                        </article>


                    </div>


                </div>
            </div>
        </div>
    </div><!-- /.content-wrapper -->




    <!-- footer -->

    @section('footer')
    @include('frontOffice.inc.footer')
    @endsection

    <!-- /footer -->
</div><!-- /.site -->

@endsection