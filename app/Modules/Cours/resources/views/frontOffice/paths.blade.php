@extends('frontOffice.layout')

@section('head')
@include('frontOffice.inc.head')
@endsection

@section('header')
@include('frontOffice.inc.header')
@endsection



@section('content')
<!-- Navigation -->
<!-- Page Content -->
<div id="body-container">

    <div>
        <style>
            .intro {
                position: relative;
            }

            .intro::after {
                content: "";
                background-color: #0a0a0a;
                opacity: 0.8;
                top: 0;
                left: 0;
                bottom: 0;
                right: 0;
                position: absolute;
                z-index: -1;
            }


            .intro::before {
                content: "";
                background: url(https://www.flexcourses.com/uploads/1569489344-1550771178-Full-Stack-Developer.jpg) no-repeat;
                background-size: cover;
                background-position: center;
                top: 0;
                left: 0;
                bottom: 0;
                right: 0;
                position: absolute;
                z-index: -1;
            }

            #pathFeatures {
                top: -40px;
            }

            .coupon-action-btn {
                border-top-left-radius: 2px !important;
                border-bottom-left-radius: 2px !important;
            }

            @media only screen and (max-width: 767px) {
                #subscriptionBlock {
                    position: fixed;
                    bottom: 0;
                    left: 0;
                    right: 0;
                    z-index: 1000000000 !important;
                    background-color: white;
                    box-shadow: 0 -.125rem .25rem rgba(0, 0, 0, .075) !important;
                    padding: 5px 16px 5px 16px !important;
                }
            }
        </style>
        <div class="intro text-white">
            <div class="container">
                <div class="row py-5 text-md-right">
                    <div class="col-12 text-center py-5">
                        <h3 class="d-inline-block mx-4 font-weight-bold pt-5">
                            مسار Full Stack Development
                        </h3>
                        <h6 class="pb-5">مسار وخطة تعليمية مخصصة لاكتساب مهارات التطوير المتكاملة Full Stack Development</h6>
                    </div>
                    <div class="col-12">
                        <div class="row align-items-center">
                            <div class="col-12 col-md-6 col-lg-4 offset-md-3 offset-lg-4">
                                <div class="row pt-3 pb-md-3 pb-4 text-center">


                                    <div class="col-12">
                                        <div id="subscriptionBlock" class="p-2 d-md-none py-3">
                                            <div class="row">
                                                <div class=" col-4 align-self-center text-center">
                                                    <div class="text-success h6 font-head font-weight-bold">
                                                        <span class="price">2353</span>
                                                        ر.س.
                                                    </div>
                                                    <div class="text-danger small strike font-head">
                                                        2835 ر.س.
                                                    </div>
                                                </div>
                                                <div class=" col-8 align-self-center">
                                                    <form method="POST" action="https://www.flexcourses.com/orders/store-path/2/guest" accept-charset="UTF-8"><input name="_token" type="hidden" value="ggo6KsqFOnjdkAUdKJwiUsQNi0d6tOr8SvWXdtNh">
                                                        <button class="btn btn-primary btn-block" type="button" data-toggle="modal" data-target="#registerModal">

                                                            اشترك الآن
                                                        </button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <form method="POST" action="https://www.flexcourses.com/orders/store-path/2/guest" accept-charset="UTF-8"><input name="_token" type="hidden" value="ggo6KsqFOnjdkAUdKJwiUsQNi0d6tOr8SvWXdtNh">
                                            <button class="btn btn-primary btn-block" type="button" data-toggle="modal" data-target="#registerModal">

                                                اشترك الآن
                                                <span class="price">2353</span>
                                                ر.س.
                                            </button>
                                        </form>
                                    </div>

                                    <div class="col-12">
                                        <div class="text-success pt-1">
                                            <div class="strike text-danger">
                                                الرسوم قبل الخصم
                                                : 2835 ر.س.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 pt-2">
                                        <div class="codeStatus text-danger small"></div>
                                        <div class="loading couponLoading d-none" style="width: 21px; height: 21px;"></div>
                                        <div class="input-group">
                                            <input class="form-control form-control-sm code" placeholder="لديك كوبون؟" name="code" type="text" value="">
                                            <div class="input-group-append">
                                                <button type="button" class="btn btn-sm btn-success shadow-sm coupon-action-btn" data-toggle="modal" data-target="#registerModal">تطبيق</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="card shadow border-0" id="pathFeatures">
                <div class="card-body py-5 px-5">
                    <div class="row">
                        <div class="col-12 col-sm-6 col-lg-3">
                            <div class="border-right h-100 pr-2 py-3">
                                <div class="font-head text-secondary"><i class="fa fa-bookmark-o pl-2"></i>
                                    عدد دورات المسار
                                </div>
                                <div class="font-weight-bold font-head">7 دورات</div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-lg-3">
                            <div class="border-right h-100 pr-2 py-3">
                                <div class="font-head text-secondary"><i class="fa fa-file-video-o pl-2"></i>
                                    عدد دروس المسار
                                </div>
                                <div class="font-weight-bold font-head">720 درس</div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-lg-3">
                            <div class="border-right h-100 pr-2 py-3">
                                <div class="font-head text-secondary"><i class="fa fa-clock-o pl-2"></i>
                                    مدة المسار
                                    <style>
                                        .tooltip.bs-tooltip-bottom .tooltip-inner,
                                        .tooltip.bs-tooltip-top .tooltip-inner {
                                            background: white !important;
                                        }

                                        .tooltip-inner {
                                            min-width: 22em !important;
                                        }

                                        .schedule ul {
                                            list-style-type: none;
                                            margin: 4px 3px 7px 0;
                                            padding: 0 0 0 0;
                                        }

                                        .schedule ul li:before {
                                            content: '✓';
                                            color: #46d6f6;
                                            font-size: 20px;
                                            line-height: 0;
                                            margin-left: 3px;
                                        }

                                        .schedule .card-body {
                                            padding: 9px !important;
                                            text-align: right;
                                        }

                                        .schedule .note {
                                            display: block;
                                            text-align: center;
                                        }
                                    </style>

                                    <i id="shecule" rel="tooltip" class="fa fa-lg fa-question-circle text-primary" data-toggle="tooltip" data-html="true" data-placement="bottom" data-title="
                                  <div class='card border-0 shadow-lg schedule'>
                                              <div class='card-header bg-info p-2 border-bottom-0 font-head-bold'>جدول مقترح</div>
                                              <div class='card-body text-dark'>
                                              <span>
                                              <ul>
                                              <li>الدراسة لمدة ساعتين يوميا حتى ترسخ المعلومات</li>
                                              <li>كل دقيقة تاخذ مابين 2.5 حتى 4 دقائق تقريبا</li>
                                              <li>مع التوقف وإعادة الدروس والتمارين والمراجعات </li>
                                              <li>مع مراجعة مشاريعك من المدربين اقترحنا الجدول</li>
                                              </ul>
                                              <span class='note'>
                                              <span class='ec ec-stars-eyes'></span>
                                              تبدأ اليوم أو بعد سنة؟ لا يوجد وقت محدد لك</span>
                                              </span>
                                              </div>
                                              </div>
                                                    ">
                                    </i>
                                </div>
                                <div class="font-weight-bold font-head">من 15 ~ 22 أسابيع | 51:11 ساعات</div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-lg-3">
                            <div class="border-right h-100 pr-2 py-3">
                                <div class="font-head text-secondary"><i class="fa fa-graduation-cap pl-2" aria-hidden="true"></i>
                                    شهادة المسار
                                </div>
                                <div class="font-weight-bold font-head">مسار Full Stack Development</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="features" class="pb-5">
                <div class="container">
                    <div class="row">
                        <div class="col-12 my-3">
                            <h3 class="text-center mb-0 text-primary">أيش يميز فلكس كورسز؟</h3>
                            <h6 class="text-center text-secondary pt-3">دورات عملية بعيدة عن الحشو والإطالات بطرق ممتعة وبالوقت المناسب لك</h6>
                        </div>

                        <div class=" col-md-6 col-lg-4 ">
                            <div class="row align-items-start">

                                <div class="col-2">
                                    <div class="circle shadow">
                                        <i class="text-secondary icon-certificate  icon-font"></i>
                                    </div>
                                </div>

                                <div class=" pt-2 pr-lg-4  col-10">
                                    <h6>ستحصل على شهادة</h6>
                                    <p class="text-secondary">احصل على شهادات تميز سيرتك الذاتية</p>
                                </div>
                            </div>
                        </div>
                        <div class=" col-md-6 col-lg-4 ">
                            <div class="row align-items-start">

                                <div class="col-2">
                                    <div class="circle shadow">
                                        <i class="text-primary icon-time  icon-font"></i>
                                    </div>
                                </div>

                                <div class=" pt-2 pr-lg-4  col-10">
                                    <h6>تعلم حسب وقتك</h6>
                                    <p class="text-secondary">اشترك الآن وتعلم بالوقت المناسب لك</p>
                                </div>
                            </div>
                        </div>
                        <div class=" col-md-6 col-lg-4 ">
                            <div class="row align-items-start">

                                <div class="col-2">
                                    <div class="circle shadow">
                                        <i class="text-secondary icon-infinity  icon-font"></i>
                                    </div>
                                </div>

                                <div class=" pt-2 pr-lg-4  col-10">
                                    <h6>دائما معك</h6>
                                    <p class="text-secondary">الدورات بتلاقيها بحسابك دائما</p>
                                </div>
                            </div>
                        </div>
                        <div class=" col-md-6 col-lg-4 ">
                            <div class="row align-items-start">

                                <div class="col-2">
                                    <div class="circle shadow">
                                        <i class="text-primary icon-follow-up  icon-font"></i>
                                    </div>
                                </div>

                                <div class=" pt-2 pr-lg-4  col-10">
                                    <h6>نتابع معك</h6>
                                    <p class="text-secondary">أثناء تعلمك راح نكون معك نساعدك ونوجهك</p>
                                </div>
                            </div>
                        </div>
                        <div class=" col-md-6 col-lg-4 ">
                            <div class="row align-items-start">

                                <div class="col-2">
                                    <div class="circle shadow">
                                        <i class="text-secondary icon-project  icon-font"></i>
                                    </div>
                                </div>

                                <div class=" pt-2 pr-lg-4  col-10">
                                    <h6>ننشر مشاريعك</h6>
                                    <p class="text-secondary">ننشر أعمالك ومشاريعك لكل زوار الموقع</p>
                                </div>
                            </div>
                        </div>
                        <div class=" col-md-6 col-lg-4 ">
                            <div class="row align-items-start">

                                <div class="col-2">
                                    <div class="circle shadow">
                                        <i class="text-primary icon-cv  icon-font"></i>
                                    </div>
                                </div>

                                <div class=" pt-2 pr-lg-4  col-10">
                                    <h6>نسوق سيرتك الذاتية</h6>
                                    <p class="text-secondary">صمم سيرتك الذاتية واترك نشرها علينا</p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="card shadow border-0">
                <div class="card-body py-5">
                    <div class="row">
                        <div class="col-12">
                            <h4 class="text-primary">ماذا يعني مسار Full Stack Development؟</h4>
                            <p>يعتبر Full Stack Development من أكثر المهارات التي يطلبها سوق العمل ويقصد بها المقدرة على الجمع مابين &quot;Front End&quot; و &quot;Back End&quot; وبمعنى أدق أن تصبح مطورا يستطيع التعامل مع تطبيقات الويب من ناحية الواجهات البرمجية التي يتفاعل معها المستخدمين و البرمجة التي يتعامل معها خادم الويب &quot;web server&quot;. سنتعلم في هذا المسار HTML, CSS, JavaScript, jQuery, Bootstrap, Git, PHP, Laravel وفي كل مادة هناك مشروع بانتظارك!</p>
                        </div>
                        <div class="col-12">
                            <div class="p-2 pb-5">
                                <h4 class="text-primary mb-4">ماذا سنتعلم في هذا المسار؟</h4>

                                <div class="row">
                                    <div class="col-12 col-md-6 col-xl-4">
                                        <div class="row">
                                            <div class="col-1 pl-0">
                                                <i class="fa fa-dot-circle-o text-danger"></i>
                                            </div>
                                            <div class="col-11">
                                                ستتعلم طرق متعددة للبرمجة
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6 col-xl-4">
                                        <div class="row">
                                            <div class="col-1 pl-0">
                                                <i class="fa fa-dot-circle-o text-danger"></i>
                                            </div>
                                            <div class="col-11">
                                                كيفية بناء صفحات الويب بلغة HTML
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6 col-xl-4">
                                        <div class="row">
                                            <div class="col-1 pl-0">
                                                <i class="fa fa-dot-circle-o text-danger"></i>
                                            </div>
                                            <div class="col-11">
                                                استخدام لغة التنسيق CSS
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6 col-xl-4">
                                        <div class="row">
                                            <div class="col-1 pl-0">
                                                <i class="fa fa-dot-circle-o text-danger"></i>
                                            </div>
                                            <div class="col-11">
                                                استخدام لغة JavaScript لبرمجة واجهات المواقع
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6 col-xl-4">
                                        <div class="row">
                                            <div class="col-1 pl-0">
                                                <i class="fa fa-dot-circle-o text-danger"></i>
                                            </div>
                                            <div class="col-11">
                                                استخدام jQuery لتسهيل التعامل مع لغة JavaScript
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6 col-xl-4">
                                        <div class="row">
                                            <div class="col-1 pl-0">
                                                <i class="fa fa-dot-circle-o text-danger"></i>
                                            </div>
                                            <div class="col-11">
                                                كيفية إدارة النسخ البرمجية باستخدام تقنية GIT
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6 col-xl-4">
                                        <div class="row">
                                            <div class="col-1 pl-0">
                                                <i class="fa fa-dot-circle-o text-danger"></i>
                                            </div>
                                            <div class="col-11">
                                                ستتعلم لغة البرمجة PHP لتبني مواقع ديناميكية
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6 col-xl-4">
                                        <div class="row">
                                            <div class="col-1 pl-0">
                                                <i class="fa fa-dot-circle-o text-danger"></i>
                                            </div>
                                            <div class="col-11">
                                                سنتعلم ماهي البرمجة الشيئية OOP
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6 col-xl-4">
                                        <div class="row">
                                            <div class="col-1 pl-0">
                                                <i class="fa fa-dot-circle-o text-danger"></i>
                                            </div>
                                            <div class="col-11">
                                                سنتعلم إطار العمل الشهير Laravel
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6 col-xl-4">
                                        <div class="row">
                                            <div class="col-1 pl-0">
                                                <i class="fa fa-dot-circle-o text-danger"></i>
                                            </div>
                                            <div class="col-11">
                                                ستتعرف على بعض ثغرات الويب وطرق الحماية منها
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6 col-xl-4">
                                        <div class="row">
                                            <div class="col-1 pl-0">
                                                <i class="fa fa-dot-circle-o text-danger"></i>
                                            </div>
                                            <div class="col-11">
                                                ستتعلم كيفية التعامل مع قواعد البيانات
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6 col-xl-4">
                                        <div class="row">
                                            <div class="col-1 pl-0">
                                                <i class="fa fa-dot-circle-o text-danger"></i>
                                            </div>
                                            <div class="col-11">
                                                طرق برمجية ونصائح مفيدة
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6 col-xl-4">
                                        <div class="row">
                                            <div class="col-1 pl-0">
                                                <i class="fa fa-dot-circle-o text-danger"></i>
                                            </div>
                                            <div class="col-11">
                                                في كل جزء من المسار سنقوم بتنفيذ مشروع مستقل
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="container" style="max-width: 1000px;">
        <h2 class="text-center text-primary pt-5">
            الدورات في مسار Full Stack Development
        </h2>
        <div class="row pb-5">
            <div class="col-md-2 col-12 text-center  circle-container  d-flex align-items-center justify-content-center">
                <div class="circle-border-light d-flex align-items-center justify-content-center">
                    <div class="circle-info d-flex align-items-center justify-content-center">
                        <div class="text-white font-head">
                            1
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-10 col-12 py-3">
                <div class="shadow-sm bg-white rounded">
                    <div class="row">
                        <div class="col-md-4 col-12 pl-md-0">
                            <div>
                                <div class="rounded-right" style="background-image: url(https://cdn.flexcourses.com/app/public/courses/images/2409/conversions/html_css-medium.jpg); height: 200px;
                                                     background-size: cover;
                                                     background-position: center center;">
                                </div>
                            </div>
                        </div>
                        <div class="p-3 col-md-8 text-md-right text-center col-12 d-flex align-items-center" style="min-height: 150px">
                            <div class="p-md-0">
                                <div class="row pl-md-5 text-secondary">
                                    <div class="col-12 pb-3">
                                        <h6 class="text-primary">
                                            <a href="#" title="دورة تصميم صفحات الإنترنت CSS HTML">دورة تصميم صفحات الإنترنت CSS HTML</a>
                                        </h6>
                                        <div>
                                            <div>
                                                سنبدأ من هنا بالأساسيات في تصميم صفحات الويب باستخدام HTML + CSS والتي تعد من أهم اللغات التي يجب عليك تعلمها قبل البدء بالتطوير.
                                                <a href="#" title="المزيد...">المزيد...</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-sm-5 col-lg-4 offset-md-0 offset-sm-1">
                                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                                        <span>من 6 ~ 9 أيام</span>
                                    </div>
                                    <div class="col-6 col-sm-5 col-lg-4">
                                        <i class="fa fa-file-text-o" aria-hidden="true"></i>
                                        <span>الدروس: 33</span>
                                    </div>
                                    <div class="col-12 text-success">
                                        الرسوم:
                                        <span>129 ر.س.</span>
                                    </div>
                                    <div class="col-12 text-danger">
                                        الرسوم الأساسية: <span class="strike">159.00 ر.س.</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-12 text-center  circle-container  d-flex align-items-center justify-content-center">
                <div class="circle-border-light d-flex align-items-center justify-content-center">
                    <div class="circle-info d-flex align-items-center justify-content-center">
                        <div class="text-white font-head">
                            2
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-10 col-12 py-3">
                <div class="shadow-sm bg-white rounded">
                    <div class="row">
                        <div class="col-md-4 col-12 pl-md-0">
                            <div>
                                <div class="rounded-right" style="background-image: url(https://cdn.flexcourses.com/app/public/courses/images/2420/conversions/js-%D9%82%D8%A7%D9%84%D8%A8-medium.jpg); height: 200px;
                                                     background-size: cover;
                                                     background-position: center center;">
                                </div>
                            </div>
                        </div>
                        <div class="p-3 col-md-8 text-md-right text-center col-12 d-flex align-items-center" style="min-height: 150px">
                            <div class="p-md-0">
                                <div class="row pl-md-5 text-secondary">
                                    <div class="col-12 pb-3">
                                        <h6 class="text-primary">
                                            <a href="#" title="دورة البرمجة باستخدام جافا سكريبت Javascript">دورة البرمجة باستخدام جافا سكريبت Javascript</a>
                                        </h6>
                                        <div>
                                            <div>
                                                ومن هنا سنتعلم أهم لغة في تطبيقات الويب وتستخدم أيضا لتطبيقات الهواتف الذكية الهجينة والتي من خلالها يمكن تنفيذ صفحات تفاعلية مع المستخدمين.
                                                <a href="#" title="المزيد...">المزيد...</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-sm-5 col-lg-4 offset-md-0 offset-sm-1">
                                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                                        <span>من 6 ~ 9 أسابيع</span>
                                    </div>
                                    <div class="col-6 col-sm-5 col-lg-4">
                                        <i class="fa fa-file-text-o" aria-hidden="true"></i>
                                        <span>الدروس: 275</span>
                                    </div>
                                    <div class="col-12 text-success">
                                        الرسوم:
                                        <span>889 ر.س.</span>
                                    </div>
                                    <div class="col-12 text-danger">
                                        الرسوم الأساسية: <span class="strike">1000.00 ر.س.</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-12 text-center  circle-container  d-flex align-items-center justify-content-center">
                <div class="circle-border-light d-flex align-items-center justify-content-center">
                    <div class="circle-info d-flex align-items-center justify-content-center">
                        <div class="text-white font-head">
                            3
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-10 col-12 py-3">
                <div class="shadow-sm bg-white rounded">
                    <div class="row">
                        <div class="col-md-4 col-12 pl-md-0">
                            <div>
                                <div class="rounded-right" style="background-image: url(https://cdn.flexcourses.com/app/public/courses/images/2406/conversions/jquery-medium.jpg); height: 200px;
                                                     background-size: cover;
                                                     background-position: center center;">
                                </div>
                            </div>
                        </div>
                        <div class="p-3 col-md-8 text-md-right text-center col-12 d-flex align-items-center" style="min-height: 150px">
                            <div class="p-md-0">
                                <div class="row pl-md-5 text-secondary">
                                    <div class="col-12 pb-3">
                                        <h6 class="text-primary">
                                            <a href="#" title="دورة jQuery جافا سكربت من الصفر للاحتراف">دورة jQuery جافا سكربت من الصفر للاحتراف</a>
                                        </h6>
                                        <div>
                                            <div>
                                                ثم سنتعلم هنا jQuery وهي مكتبة JavaScript غايتها تبسيط اللغة واختصار الجمل البرمجية الطويلة والمكررة مع خصائصها الإضافية مثل المؤثرات البصرية.
                                                <a href="#" title="المزيد...">المزيد...</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-sm-5 col-lg-4 offset-md-0 offset-sm-1">
                                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                                        <span>من 8 ~ 12 أيام</span>
                                    </div>
                                    <div class="col-6 col-sm-5 col-lg-4">
                                        <i class="fa fa-file-text-o" aria-hidden="true"></i>
                                        <span>الدروس: 43</span>
                                    </div>
                                    <div class="col-12 text-success">
                                        الرسوم:
                                        <span>169 ر.س.</span>
                                    </div>
                                    <div class="col-12 text-danger">
                                        الرسوم الأساسية: <span class="strike">259.00 ر.س.</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-12 text-center  circle-container  d-flex align-items-center justify-content-center">
                <div class="circle-border-light d-flex align-items-center justify-content-center">
                    <div class="circle-info d-flex align-items-center justify-content-center">
                        <div class="text-white font-head">
                            4
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-10 col-12 py-3">
                <div class="shadow-sm bg-white rounded">
                    <div class="row">
                        <div class="col-md-4 col-12 pl-md-0">
                            <div>
                                <div class="rounded-right" style="background-image: url(https://cdn.flexcourses.com/app/public/courses/images/2422/conversions/bootstrap-%D9%82%D8%A7%D9%84%D8%A8-medium.jpg); height: 200px;
                                                     background-size: cover;
                                                     background-position: center center;">
                                </div>
                            </div>
                        </div>
                        <div class="p-3 col-md-8 text-md-right text-center col-12 d-flex align-items-center" style="min-height: 150px">
                            <div class="p-md-0">
                                <div class="row pl-md-5 text-secondary">
                                    <div class="col-12 pb-3">
                                        <h6 class="text-primary">
                                            <a href="#" title="دورة بناء صفحات الإنترنت المرنة باستخدام Bootstrap V4">دورة بناء صفحات الإنترنت المرنة باستخدام Bootstrap V4</a>
                                        </h6>
                                        <div>
                                            <div>
                                                ثم سنصل لمرحلة كيف تصبح تطبيقات الويب متوافقة مع جميع أحجام الشاشات وذات تصميم رائع ومبسط باستخدام Bootstrap
                                                <a href="#" title="المزيد...">المزيد...</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6 col-sm-5 col-lg-4 offset-md-0 offset-sm-1">
                                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                                        <span>من 6 ~ 9 أيام</span>
                                    </div>
                                    <div class="col-6 col-sm-5 col-lg-4">
                                        <i class="fa fa-file-text-o" aria-hidden="true"></i>
                                        <span>الدروس: 23</span>
                                    </div>
                                    <div class="col-12 text-success">
                                        الرسوم:
                                        <span>129 ر.س.</span>
                                    </div>
                                    <div class="col-12 text-danger">
                                        الرسوم الأساسية: <span class="strike">159.00 ر.س.</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-12 text-center circle-container-sm d-flex align-items-center justify-content-center">
                <div class="circle-border-light d-flex align-items-center justify-content-center">
                    <div class="circle-primary d-flex align-items-center justify-content-center">
                        <div class="text-white font-head">
                            <i class="fa fa-flag"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-10 col-12 py-3">
                <div class="card border-0 d-flex align-items-center justify-content-center" style="min-height: 150px">
                    <div class="p-4 text-justify">
                        وبعد أن انتهيت الآن من هذه المواد السابقة وقمت بتطبيقها عمليا أصبح الآن لديك مهارات Full Stack Development وستحصل على شهادة بذلك تحمل اسمك والمسار الذي قمت بدراسته وعدد الساعات التي أمضيتها في هذا المسار.
                    </div>
                </div>
            </div>
        </div>
    </div>

   <!-- <div id="registerModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
        <div class="modal-dialog modal-md mt-5" role="document">

            <div class="modal-content">
                <button type="button" class="close login-register-modal-close rounded-circle bg-white shadow-sm" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times" aria-hidden="true"></i>
                </button>
                <ul class="nav nav-tabs nav-fill" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="login-tab" data-toggle="tab" title="دخول" role="tab" href="#login">دخول</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="register-tab" data-toggle="tab" title="تسجيل" role="tab" href="#register">تسجيل</a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane show active" id="login" role="tabpanel">
                        <div class="card p-3 p-md-5 rounded-0 border-top-0">
                            <div class="panel-body pt-3">


                                <form class="form-horizontal" role="form" method="POST" action="https://www.flexcourses.com/login">
                                    <input type="hidden" name="_token" value="ggo6KsqFOnjdkAUdKJwiUsQNi0d6tOr8SvWXdtNh">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <input type="email" class="form-control py-md-4 rounded-0 text-right" name="email" placeholder="البريد الإلكتروني" value="" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <input type="password" class="form-control py-md-4 rounded-0 text-right" placeholder="كلمة المرور" name="password" required>
                                        </div>
                                    </div>

                                    <div class="form-group mb-0">
                                        <div class="col-md-12 text-center">
                                            <label>
                                                <input type="checkbox" name="remember"> تذكرني
                                            </label>
                                        </div>
                                    </div>

                                    <div class="small p-3">
                                        بدخولك لحسابك فإنك توافق على <a href="page/privacy-policy">سياسة الخصوصية</a> و<a href="page/terms">شروط الاستخدام</a>.
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <button type="submit" class="btn btn-block btn-primary">دخول</button>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-12 pb-md-4 text-center">
                                            <a href="#" title="هل نسيت كلمة المرور؟">هل نسيت كلمة المرور؟</a>
                                        </div>
                                    </div>
                                </form>

                                <hr>

                                <p class="text-secondary">
                                    <small>أو بإمكانك الدخول باستخدام:</small>
                                </p>
                                <style>
                                    .btn-twitter {
                                        border: 1px solid #1DA1F2;
                                        color: #1DA1F2;
                                        display: block;
                                        background: url('/imgs/t2-logo.png') white;
                                    }

                                    .btn-google {
                                        border: 1px solid #EA4335;
                                        color: #EA4335;
                                        display: block;
                                        background: url('/imgs/g-logo.svg') white;
                                    }

                                    .btn-twitter:hover {
                                        color: #1DA1F2;
                                    }

                                    .btn-google:hover {
                                        color: #EA4335;
                                    }

                                    .social-btn {
                                        background-position: right 7px center;
                                        background-repeat: no-repeat;
                                        background-size: 28px;
                                        padding-right: 49px;
                                        text-align: right;
                                        margin-bottom: 11px;
                                    }

                                    .social-btn:hover {
                                        opacity: 0.8;
                                    }
                                </style>

                                <div class="row">
                                    <div class="col-md-6">
                                        <a href="#" title="تويتر" class="social-btn btn btn-twitter">
                                            تويتر
                                        </a>
                                    </div>
                                    <div class="col-md-6">
                                        <a href="#" title="جوجل" class="social-btn btn btn-google">
                                            جوجل
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="register" role="tabpanel">
                        <div class="card p-3 p-md-5 rounded-0 border-top-0">
                            <div class="panel-body pt-3">

                                <form class="form-horizontal" role="form" method="POST" action="https://www.flexcourses.com/register">
                                    <input type="hidden" name="_token" value="ggo6KsqFOnjdkAUdKJwiUsQNi0d6tOr8SvWXdtNh">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <input id="name" placeholder="الاسم" type="text" class="form-control py-md-4 rounded-0 text-right" name="name" value="" required>
                                            <span class="small text-secondary">الاسم كاملا (يظهر للإدارة وفي شهاداتك فقط)</span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <input id="name" placeholder="اسم المستخدم" type="text" class="form-control py-md-4 rounded-0 text-right" name="nickname" value="" required>
                                            <span class="small text-secondary">اسم العرض الخاص بك (أحرف وأرقام إنجليزية فقط)</span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <input id="email" placeholder="البريد الإلكتروني" type="email" class="form-control py-md-4 rounded-0 text-right" name="email" value="" required>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <input placeholder="كلمة المرور" id="password" type="password" class="form-control py-md-4 rounded-0 text-right" name="password" required>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <input id="password-confirm" placeholder="تأكيد كلمة المرور" type="password" class="form-control py-md-4 rounded-0 text-right" name="password_confirmation" required>
                                        </div>
                                    </div>

                                    <div class="small p-3">
                                        بتسجيلك فإنك توافق على <a href="page/privacy-policy">سياسة الخصوصية</a> و<a href="page/terms">شروط الاستخدام</a>.
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <button type="submit" class="btn btn-block btn-primary">
                                                تسجيل
                                            </button>
                                        </div>
                                        <div class="col-md-12 pt-3 pb-md-4 text-center">
                                            <a class="register-link" title="مسجل مسبقا؟ سجل دخولك من هنا." href="#login">مسجل مسبقا؟ سجل دخولك من هنا.</a>
                                        </div>
                                    </div>
                                </form>

                                <hr>

                                <p class="text-secondary">
                                    <small>أو بإمكانك الدخول باستخدام:</small>
                                </p>
                                <style>
                                    .btn-twitter {
                                        border: 1px solid #1DA1F2;
                                        color: #1DA1F2;
                                        display: block;
                                        background: url('/imgs/t2-logo.png') white;
                                    }

                                    .btn-google {
                                        border: 1px solid #EA4335;
                                        color: #EA4335;
                                        display: block;
                                        background: url('/imgs/g-logo.svg') white;
                                    }

                                    .btn-twitter:hover {
                                        color: #1DA1F2;
                                    }

                                    .btn-google:hover {
                                        color: #EA4335;
                                    }

                                    .social-btn {
                                        background-position: right 7px center;
                                        background-repeat: no-repeat;
                                        background-size: 28px;
                                        padding-right: 49px;
                                        text-align: right;
                                        margin-bottom: 11px;
                                    }

                                    .social-btn:hover {
                                        opacity: 0.8;
                                    }
                                </style>

                                <div class="row">
                                    <div class="col-md-6">
                                        <a href="#" title="تويتر" class="social-btn btn btn-twitter">
                                            تويتر
                                        </a>
                                    </div>
                                    <div class="col-md-6">
                                        <a href="#" title="جوجل" class="social-btn btn btn-google">
                                            جوجل
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>-->

</div>
<!-- /.container -->
@endsection