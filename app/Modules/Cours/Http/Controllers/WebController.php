<?php

namespace App\Modules\Cours\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class WebController extends Controller
{

  public function showCourses()
  {
      return view('Cours::frontOffice.courses'); 
  }


  public function showPaths()
  {
      return view('Cours::frontOffice.paths'); 
  }

  public function showPacks()
  {
      return view('Cours::frontOffice.packs'); 
  }


  public function showManagerCourses()
  {
      return view('Cours::backOffice.courses'); 
  }
}
