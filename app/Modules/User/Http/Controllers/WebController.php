<?php

namespace App\Modules\User\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class WebController extends Controller
{

    public function showLogin()
    {
        return view('User::frontOffice.login'); 
    }

    public function showRegister()
    {
        return view('User::frontOffice.register'); 
    }

    public function showManagerUsers()
    {
        return view('User::backOffice.list'); 
    }
}
