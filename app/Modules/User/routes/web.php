<?php

Route::get('/login', 'WebController@showLogin')->name('showLogin');
Route::get('/register', 'WebController@showRegister')->name('showRegister');

Route::get('/manager/users', 'WebController@showManagerUsers')->name('showManagerUsers'); 
