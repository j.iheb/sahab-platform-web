
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
   <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0" />
   <title> التسجسل في منصة سحاب </title>
    
   <link rel="stylesheet" media="screen" href="{{asset('css/frontOffice/login.css')}}" />
      <meta name="description" content="Sign in to your  sahab platform">

    <meta name="segment-controller" content="signups" />
    <meta name="segment-action" content="new" />
    <meta name="itly-controller" content="signups" />
    <meta name="itly-action" content="new" />


  </head>

  <body class="logged-out not-pro not-player not-self not-team not-on-team  sign-up" style="direction:rtl;">
    
    <div id="main-container">
      <section class="auth-sidebar">
        <div class="auth-sidebar-content">
          <header>
            <a href="{{route('showHome')}}" class="logo">
                <img src="{{asset('img/sahab-logo.png')}}" alt="" style="width:200px;">
            </a>
            <h1> إكتشف أفضل الدورات و المدرسين.</h1>
          </header>
          <div class="artwork">
            <div class="artwork-image"></div>
              <p class="artwork-attribution">
              

              </p>
          </div>
        </div>
      </section>
      <section class="content">
          <nav class="auth-nav">
              <p class="auth-link">
     تملك حساب ? <a href="{{route('showLogin')}}"> تسجيل الدخول</a>
  </p>

          </nav>
        <main>
          



<div class="auth-content">
  <h2>سجل في منصة سحاب</h2>
    <div class="auth-connections">
        
<form class="auth-google-form" action="/auth/google_signup" accept-charset="UTF-8" method="post"><input name="utf8" type="hidden" value="&#x2713;" /><input type="hidden" name="authenticity_token" value="6WMbpKvXJ1k5g6M991cyCNt69sWaAksaag00KMs7VNTGzDIXCOAFUNJnuCZ/h172FP+tkL1VD7hBWeSL3kR+Xg==" />
  <button name="button" type="submit" class="form-btn auth-google auth-google-new" data-auth-action="Sign Up">
    <svg xmlns="http://www.w3.org/2000/svg" aria-labelledby="gj0zivfpx3cm873x8madhxpn52pwh26" role="img" viewBox="0 0 24 24" class="icon "><title id="gj0zivfpx3cm873x8madhxpn52pwh26">Google icon</title><path d="M12.24 10.285V14.4h6.806c-.275 1.765-2.056 5.174-6.806 5.174-4.095 0-7.439-3.389-7.439-7.574s3.345-7.574 7.439-7.574c2.33 0 3.891.989 4.785 1.849l3.254-3.138C18.189 1.186 15.479 0 12.24 0c-6.635 0-12 5.365-12 12s5.365 12 12 12c6.926 0 11.52-4.869 11.52-11.726 0-.788-.085-1.39-.189-1.989H12.24z"></path></svg>

   جوجل
</button></form>
      <a class="auth-twitter form-btn" data-auth-action="Sign Up" rel="nofollow" data-method="post" href="https://dribbble.com/auth/Twitter?signup=true">
        <svg xmlns="http://www.w3.org/2000/svg" aria-labelledby="5xeh44kpk3zmmkhfweabwnygrdb7bx5" role="img" viewBox="0 0 24 24" class="icon "><title id="5xeh44kpk3zmmkhfweabwnygrdb7bx5">Twitter icon</title><path d="M23.954 4.569c-.885.389-1.83.654-2.825.775 1.014-.611 1.794-1.574 2.163-2.723-.951.555-2.005.959-3.127 1.184-.896-.959-2.173-1.559-3.591-1.559-2.717 0-4.92 2.203-4.92 4.917 0 .39.045.765.127 1.124C7.691 8.094 4.066 6.13 1.64 3.161c-.427.722-.666 1.561-.666 2.475 0 1.71.87 3.213 2.188 4.096-.807-.026-1.566-.248-2.228-.616v.061c0 2.385 1.693 4.374 3.946 4.827-.413.111-.849.171-1.296.171-.314 0-.615-.03-.916-.086.631 1.953 2.445 3.377 4.604 3.417-1.68 1.319-3.809 2.105-6.102 2.105-.39 0-.779-.023-1.17-.067 2.189 1.394 4.768 2.209 7.557 2.209 9.054 0 13.999-7.496 13.999-13.986 0-.209 0-.42-.015-.63.961-.689 1.8-1.56 2.46-2.548l-.047-.02z"></path></svg>

</a>    </div>
    <hr class='divider'>

  <div class="auth-form signup-form">
    <form class="hide-remove gen-form with-messages signup" id="new_user" action="https://dribbble.com/signup" accept-charset="UTF-8" method="post"><input name="utf8" type="hidden" value="&#x2713;" /><input type="hidden" name="authenticity_token" value="fL5RAhXgICNIyP/5np97zSXeVbAOvq+EDBBhRNB3RDRTEXixttcCKqMs5OIWTxcz6lsO5Snp6yYnRLHnxQhuvg==" />
      <div class="form-field hide">
  <fieldset>
    <label for="contact_surname">إسم المستعمل</label>
    <input type="text" name="surname" id="surname" />
  </fieldset>
</div>


      


      <div class="form-field-group">
        <div class="form-field">
          <fieldset class="user_name"><label for="user_name">الأسم</label><input autocomplete="name" class="text-input" type="text" name="user[name]" id="user_name" /></fieldset>
        </div>

        <div class="form-field">
          <fieldset class="user_login"><label for="user_login">إسم المستعمل</label><input autocorrect="off" autocapitalize="off" autocomplete="username" class="text-input" type="text" name="user[login]" id="user_login" /></fieldset>
        </div>
      </div>

      <div class="form-field">
        <fieldset class="user_email"><label for="user_email">البريد الإلكتروني</label><input class="text-input" type="text" name="user[email]" id="user_email" /></fieldset>
      </div>

      <div class="form-field">
        <fieldset class="user_password"><label for="user_password">كلمة السر</label><input class="text-input" placeholder="6+ characters" type="password" name="user[password]" id="user_password" /></fieldset>
      </div>

      <div class="form-field check-wrap opt-in">
        <fieldset>
          <input type="checkbox" id="user_agree_to_terms" name="user[agree_to_terms]" />
          <label for="user_agree_to_terms">
            إنشاء حساب يعني قبولك ب
            <a target="_blank" href="#">قانون الإستعمال</a>
          </label>
        </fieldset>
      </div>

      <div class="form-btns">
        <input type="submit" name="commit" value="Create Account" class="button form-sub" data-disable-with="Create Account" />
      </div>

        <p class="auth-link-mobile">
          لديك حساب? <a href="{{route('showLogin')}}"> سجل الدخول</a>
        </p>

    


        

</form>  </div>
</div>

        </main>
      </section>
    </div>





  </body>
</html>
