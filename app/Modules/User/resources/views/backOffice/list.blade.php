@extends('backOffice.layout')

@section('head')
@include('backOffice.inc.head',
['title' => 'Dashboard',
'description' => 'Espace Administratif - '
])
@endsection

@section('header')
@include('backOffice.inc.header')
@endsection

@section('sidebar')
@include('backOffice.inc.sidebar', [
'current' => 'users'
])
@endsection

@section('content')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins') }}/datatable/datatable.css">
<script type="text/javascript" src="{{ asset('plugins') }}/datatable/datatable.js"></script>

<script type="text/javascript">
    /* Datatables responsive */

    $(document).ready(function() {
        $('#datatable-responsive').DataTable({
            responsive: true,
            language: {
                url: "{{ asset('plugins/datatable/lang/'.\Illuminate\Support\Facades\Session::get('youfors_applocale').'.js') }}"
            }
        });
        $('.dataTables_filter input').attr("placeholder", "{{ trans('lang.search') }}...");
    });
</script>


<div class="breadcrumb">
    <h1>{{ trans('lang.users') }}</h1>
    <ul>
        <li><a href="{{ route('showManagerHome') }}">{{ trans('lang.dashboard') }}</a></li>
        <li>{{ trans('lang.users') }}</li>
    </ul>
</div>

<div class="separator-breadcrumb border-top"></div>

<div class="row mb-4">

    <div class="col-md-12 mb-4">
        <div class="card text-left">

            <div class="card-body">
                <table id="datatable-responsive" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>{{ trans('lang.full_name') }}</th>
                            <th>{{ trans('lang.email') }}</th>
                            <th>{{ trans('lang.status') }}</th>
                            <th>{{ trans('lang.roles') }}</th>
                            <th>{{ trans('lang.date') }}</th>
                            <th>{{ trans('lang.action') }}</th>
                        </tr>
                    </thead>

                    <tfoot>
                        <tr>
                            <th>{{ trans('lang.full_name') }}</th>
                            <th>{{ trans('lang.email') }}</th>
                            <th>{{ trans('lang.status') }}</th>
                            <th>{{ trans('lang.roles') }}</th>
                            <th>{{ trans('lang.date') }}</th>
                            <th>{{ trans('lang.action') }}</th>
                        </tr>
                    </tfoot>

                    <tbody>
                        @for ($i = 0; $i < 6; $i++) <tr>
                            <td> j.iheb </td>
                            <td> me.jabri.iheb@gmail.com </td>
                            <td>
                                <span class="badge badge-pill badge-outline-secondary p-2 m-1"> Active </span>
                            </td>
                            <td>

                                <span class="badge badge-pill p-2 m-1  badge-outline-primary  ">User
                                </span> &nbsp;

                            </td>
                            <td> 16-03-2021</td>
                            <td>
                                @if(1 !== 3)
                                <span class="badge badge-pill badge-outline-danger p-2 m-1" title="{{ trans('lang.user_ban_action') }}"><i class="nav-icon i-Lock"></i></span>
                                @else
                                <span class="badge badge-pill badge-outline-info p-2 m-1" title="{{ trans('lang.user_authorize_action') }}"><i class="nav-icon i-Unlock"></i></span>
                                @endif
                                @if( 1 == 1)
                                <span class="badge badge-pill badge-outline-warning p-2 m-1" title="{{ trans('lang.user_revoke_action') }}"><i class="nav-icon i-Remove-User"></i></span>
                                @else
                                <span class="badge badge-pill badge-outline-danger p-2 m-1" title="{{ trans('lang.user_admin_action') }}"><i class="nav-icon i-Add-User"></i></span>
                                @endif

                            </td>
                            </tr>
                            @endfor
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>

@endsection